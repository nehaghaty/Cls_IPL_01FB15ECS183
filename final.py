import sys
import os
import yaml
import random
import pandas as pd

ifile = 980901

fcount = int(input("Enter the match to simulate: "))
fcount = fcount - 1

bat = open('batCluster.txt','r')
bowl = open('bowlCluster.txt','r')
i1_batsman_runs = dict()
i2_batsman_runs = dict()
dbat = {}
dbowl = {}

#dictionary with keys as batsman and values as cluter id
for i in bat.readlines():
	d = i.split(",")
	for i in d[1:]:
		dbat[i.strip()] = d[0]

for i in bowl.readlines():
	d = i.split(",")
	for i in d[1:]:
		dbowl[i.strip()] = d[0]
	

winner = ""

wicks = 0
fopen = open("Predicted/MatchPrediction"+str(fcount+1)+".txt",'w')
#to predict the run scored for the ball 
def predictRun(plist):
	r = random.random() # r is a random number generated between 0 and 1
	tl = [sum(plist[:i]) for i in range(len(plist)+1)]
	##print(tl) tl is the cumulative distribution of probabilities for different runs scored , inlcudes wickets as well	
	tl = tl[1:]
	i = 0
	run = 0
	if r==1:
		return run,1 #run,wicket(1->wicket taken)
	else:
		while r > tl[i] :
			if run==6:
				run=0
				##print "Wicket!"
				#fopen.write("Wicket!\n")
				return run,1
			elif run == 4:
				run = run + 2
			else:
				run = run + 1
			i = i + 1

		return run,0  #run,wicket(0->no wicket)


#ball by ball simulation
def play(innings):
	global count 
	global trun
	global target
	global innings1
	global innings2
	global striker
	global non_striker
	wicks = 0
	wicket = 0
	count = 0
	batsman_runs = ""
	if(innings == innings1):
		batsman_runs = i1_batsman_runs
		x=0 #marks the first innings
	
	else:
		batsman_runs = i2_batsman_runs
		x=11
	

	global winner
	over=0
	batsman_order=list()
	bowler_order = list()

	batsman_order_file = open('batsman/Match'+str(fcount+1)+'.txt','r') #replace x here by the respective match number
	bowler_order_file = open('bowler/Match'+str(fcount+1)+'.txt', 'r')
	#traverse through to get order. firstpick 2 batsman
	for i in batsman_order_file:
		i = i.strip()
		if x!='':			
			batsman_order.append(i)
	if '\xef\xbb\xbf1st innings' in batsman_order:	
		batsman_order.remove('\xef\xbb\xbf1st innings')
	else:
		batsman_order.remove('1st innings')
	batsman_order.remove('2nd innings')
	##print batsman_order

	for i in bowler_order_file:
		i = i.strip()
		if x!='':
			bowler_order.append(i)

	bowler_order.remove('1st innings')
	bowler_order.remove('2nd innings')
	##print(bowler_order)	

	if x==0:
		batsman_order = batsman_order[1:12]
		bowler_order = bowler_order[1:21]
		#print "innings1 " + str(batsman_order)
		#print "innings2 " + str(bowler_order)
	else:
		batsman_order = batsman_order[14:25]
		bowler_order = bowler_order[23:43]
		#print "innings2 " + str(batsman_order)
		#print "innings2 " + str(bowler_order)
		# batsman_order and bowler order for respective innings
	
	striker = batsman_order[0]
	non_striker = batsman_order[1]
	bowler = bowler_order[0]
	fopen.write("Over: "+str(over+1)+'\n')
	ballCount = 0

	for row in range(0,120):
		count = count + 1
		ballCount += 1
		if (count%7 == 0): #marks the change in over
			temp=striker
			striker=non_striker
			non_striker=temp
			over+=1
			count+=1
			ballCount = 1
			#print over
			bowler = bowler_order[over]
			#print "over "+str(over+1)
			fopen.write("Over: "+str(over+1)+'\n')
			
		try:
			a = dbat[striker] #gives the cluster id for striker batsman
		except:
			a = 1
			#print "nf:"+striker
			fopen.write("nf:"+str(striker)+'\n')

		try:
			b = dbowl[bowler] #gives the cluster id for bowler
		except:
			b = 1
			#print "nf:"+bowler
			fopen.write("nf:"+str(bowler)+'\n')

		df = pd.read_csv('Probabilities.csv')
		for i in range(len(df)):
			c = str(df.iloc[i][0])#getting the cluster id of batsman and bowler from probabilties.csv
			cBat,cBowl = c.split('_')
			if(a==cBat and b==cBowl): #only if cluster id matches then upload the probabilities
				p0 = df.iloc[i][3]
				p1 = df.iloc[i][4]
				p2 = df.iloc[i][5]
				p3 = df.iloc[i][6]
				p4 = df.iloc[i][7]
				p6 = df.iloc[i][8]
				pw = df.iloc[i][9]
				break
			else:
				p0 = p1 = p2 = p3 = p4 = p6 = pw = 0.1428 #equally likely probability for those players not found in clusters
		p,w = predictRun([p0,p1,p2,p3,p4,p6,pw])

		batsman_runs[striker] = batsman_runs.get(striker,0) + p

		trun = trun + p
		
		wicks = wicks + w	 

		#print("** Ball - " + str(count) )
		#print("\n** Run - " + str(p) + '\n' )
		#print("\n** TOTAL Run - " + str(trun) + "  Wickets - " + str(wicks) + '\n' )

		fopen.write("\tBall - " + str(ballCount))
		fopen.write("\tRun - " + str(p)+'\n')
		fopen.write("\tCurrent Score - " + str(trun) + "/" + str(wicks) + '\n' )
	
		if(w!=1): #if wicket wasnt taken

			if(p%2==0):
				#even runs, keep same batsman
				#print "striker batsman : "+striker
				#print "non striker batsman : "+non_striker
				fopen.write("\tStriker : "+str(striker)+'\n')
				fopen.write("\tNon Striker : "+str(non_striker)+'\n')
			
			elif(p%2==1):
				#odd runs, swap order between batsman
				temp=striker
				striker=non_striker
				non_striker=temp
				#print "striker batsman : "+striker
				#print "non striker batsman : "+non_striker
				fopen.write("\tStriker : "+str(striker)+'\n')
				fopen.write("\tNon Striker : "+str(non_striker)+'\n')

		else:
			#get next batsamn in order, dismiss the present batsman
			if wicks==10:
				fopen.write("\n\tAll Out!")
				break
			else:				
				striker=batsman_order[wicket+2]
				wicket+=1;
		
				#print "wicket!!!! "
				#print "striker batsman : "+striker
				#print "non striker batsman : "+non_striker

				fopen.write("\tWicket!!!! "+'\n')
				fopen.write("\tStriker : "+str(striker)+'\n')
				fopen.write("\tNon Striker : "+str(non_striker)+'\n')
				


		##print "batsman : "+row[row.keys()[0]]['batsman']
		#print "bowler : "+bowler
		#print "\n\n"
		fopen.write("\tBowler : "+str(bowler)+'\n')
		fopen.write("\n\n")
		
		if(innings == innings2 and trun > target):
			winner = "2"
			break


y = str(ifile+fcount*2)+'.yaml'
y = 'cricsheets/' + y
data = yaml.load(open(y,'r').read())
count = 0
trun = 0

fopen.write("Match No. "+str(fcount+1)+'\n')
fopen.write("Date: "+str(data['info']['dates'][0])+'\n')
fopen.write(str(data['info']['teams'][0])+' VS '+str(data['info']['teams'][1])+'\n\n')
fopen.write("Toss: \n")
fopen.write(str(data['info']['toss']['winner'])+' won the toss and chose to ')
fopen.write(str(data['info']['toss']['decision'])+'\n\n')

innings1 = data['innings'][0]['1st innings']['deliveries']
innings2 = data['innings'][1]['2nd innings']['deliveries']

fopen.write("################################## Innings 1 ######################################\n\n")

play(innings1)

target = trun
count = 0
trun = 0

fopen.write("################################## Innings 2 ######################################\n\n")

play(innings2)


#print "PREDICTED : "
#print "\nTarget  : " + str(target)
#print "Score  : " + str(trun)
fopen.write("PREDICTED : \n")
fopen.write("\nTeam 1 Score  : " + str(target))
fopen.write("\nTeam 2 Score  : " + str(trun))


fopen.write("\n")
if(winner != "2"):
	#print "Winner :Team 1"
	fopen.write("\nWinner :Team 1"+"\n")

else:
	#print "Winner : Team 2 "
	fopen.write("\nWinner :Team 2"+"\n")
#print "\n"

top3 = sorted(i1_batsman_runs, key = i1_batsman_runs.__getitem__, reverse = True)
#print "Team 1"
fopen.write("\nTeam 1"+"\n")
for i in top3:
	#print i,"-",i1_batsman_runs[i]
	fopen.write(str(i)+"-"+str(i1_batsman_runs[i])+'\n')
#print "\n\n"
fopen.write("\n")
top3 = sorted(i2_batsman_runs, key = i2_batsman_runs.__getitem__, reverse = True)

#print "Team 2"
fopen.write("\nTeam 2"+"\n")
for i in top3:
	#print i,"-",i2_batsman_runs[i]
	fopen.write(str(i)+"-"+str(i2_batsman_runs[i])+'\n')

def maxrunplayer(y):
	data = yaml.load(open(y,'r').read())


	innings1 = data['innings'][0]['1st innings']['deliveries']
	innings2 = data['innings'][1]['2nd innings']['deliveries']


	in1_score = 0
	in2_score = 0

	i1_batsman_runs = dict()
	i2_batsman_runs = dict()

	for row in innings1:
		in1_score += row[row.keys()[0]]['runs']['total']
		try:
			i1_batsman_runs[row[row.keys()[0]]['batsman']] += row[row.keys()[0]]['runs']['batsman']
		except KeyError:
			i1_batsman_runs[row[row.keys()[0]]['batsman']] = 0

	for row in innings2:
		in2_score += row[row.keys()[0]]['runs']['total']
		try:
			i2_batsman_runs[row[row.keys()[0]]['batsman']] += row[row.keys()[0]]['runs']['batsman']
		except KeyError:
			i2_batsman_runs[row[row.keys()[0]]['batsman']] = 0

	#print "\n\n\nACTUAL : \n"
	#print "Team 1",in1_score
	#print "Team 2",in2_score

	fopen.write("\nACTUAL : \n")
	fopen.write("Team 1 "+str(in1_score)+'\n')
	fopen.write("Team 2 "+str(in2_score)+'\n')

	#print "\n\n"
	fopen.write("\n")
	top = sorted(i1_batsman_runs, key = i1_batsman_runs.__getitem__, reverse = True)[0]
	#print "Innings 1 ",top,"-",i1_batsman_runs[top]
	fopen.write("Innings 1 "+str(top)+"-"+str(i1_batsman_runs[top])+'\n')

	top = sorted(i2_batsman_runs, key = i2_batsman_runs.__getitem__, reverse = True)[0]
	#print "Innings 2 ",top,"-",i2_batsman_runs[top]
	fopen.write("Innings 2 "+str(top)+"-"+str(i2_batsman_runs[top])+'\n')

maxrunplayer(y)
fopen.close()
