f = open("data.txt","r")

data = []
for x in f:
	x = x.strip()
	x = x.split('\t')
	x[0] = int(x[0])
	data.append(x)

f.close()

from operator import itemgetter

data.sort(key=itemgetter(0))

f = open("data.txt","w")

for i in data:
	print(str(i[0])+'\t'+str(i[1])+'\t'+i[2]+'\t'+i[3])
	f.write(str(i[0])+'\t'+str(i[1])+'\t'+str(i[2])+'\t'+str(i[3])+'\n')

f.close()