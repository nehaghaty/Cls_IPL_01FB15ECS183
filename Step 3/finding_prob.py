import csv
d={}
#d.setdefault(key, []).append(value)
f1=open("output.csv","r")
    #csvreader = csv.reader(csvfile, delimiter=',', quotechar='|')
for row in f1:
    #d[row[-1]].append(row)
    row = row.strip()
    r=row.split(',') 
    if r[-1] not in d.keys():    
        d[r[-1]] = [] 
    d[r[-1]].append(r)

        
f = open("data.txt",'w')
for key in d:
    no_of_balls_faced = 0
    no_runs = 0
    singles = 0
    two_s = 0
    three_s = 0
    four_s = 0
    six_s = 0
    wicket_taken = 0
    print(d[key])
    
    for i in d[key]:
        no_of_balls_faced += int(i[2])
        no_runs += int(i[3])
        singles += int(i[4])
        two_s += int(i[5])
        three_s += int(i[6])
        four_s += int(i[7])
        six_s += int(i[8])
        wicket_taken += int(i[9])
    prob_runs = float((singles+two_s+three_s+four_s+six_s)/float((no_of_balls_faced)))
    prob_out = (wicket_taken)/float((no_of_balls_faced))
    f.write(key + '\t' + str(prob_runs) + '\t' +str(prob_out)+'\n')
        
f.close()
        
        
        