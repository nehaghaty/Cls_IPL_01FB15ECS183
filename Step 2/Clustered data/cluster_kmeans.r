#code for kmeans clustering
#http://www.sthda.com/english/articles/25-cluster-analysis-in-r-practical-guide/111-types-of-clustering-methods-overview-and-quick-start-r-code/
#install.packages("factoextra")
#install.packages("cluster")
#install.packages("magrittr")
library("cluster")
library("factoextra")
library("magrittr")

#remove or take care of missing values
dataset[is.na(dataset)] <- 0
#convert all columns to numeric
dataset$Average <- as.numeric(dataset$Average)
dataset$Strike_Rate <- as.numeric(dataset$Strike_Rate)
#to convert categorical variable to numeric, do
dataset$Batsman <- as.numeric(as.factor(dataset$Batsman))
#create a separate dataset that holds id for each batsman
#few Na's may be introduced due to coeercion, replace them as well
dataset[is.na(dataset)] <- 0
                                 
#distance measures
res.dist <- get_dist(dataset, stand = TRUE, method = "pearson")
fviz_dist(res.dist, 
          gradient = list(low = "#00AFBB", mid = "white", high = "#FC4E07"))

#partitioning clustering
set.seed(123)
#to make 10 clusters
km.res <- kmeans(dataset, 10, nstart = 25)
# Visualize
library("factoextra")
fviz_cluster(km.res, data = dataset,
             ellipse.type = "convex",
             palette = "jco",
             ggtheme = theme_minimal(),
             xlab="x value",
             ylab="y value",
             main="Overall cluster plot for batsman")
write.table(data_names, "D:/5th sem/Big DAta/mydata.txt", sep="\t")
Cluster <- Average_cluster$cluster
write.csv(Cluster,file="D:/5th sem/Big DAta/Average_cluster.csv")