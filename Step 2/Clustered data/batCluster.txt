1,CH Morris,CR Brathwaite,DS Kulkarni,ER Dwivedi,AD Russell,KH Pandya,JA Morkel,AB de Villiers,BCJ Cutting,Bipul Sharma,A Ashish Reddy,B Kumar
2,JP Duminy,KK Nair,KD Karthik,JP Faulkner,MP Stoinis,M Vijay,M Vohra,P Sahu,KJ Abbott,KC Cariappa,MK Pandey,JO Holder,N Rana,KA Pollard,JC Buttler,MJ McClenaghan,MS Dhoni,KP Pietersen,NLTC Perera,KL Rahul,KM Jadhav,Iqbal Abdulla
3,Imran Tahir,J Yadav,Ishan Kishan,AD Nath,DW Steyn,F Behardien,C Munro,JJ Bumrah,HH Pandya,GJ Bailey,IK Pathan,Ankit Sharma,CJ Jordan,AP Tare,BB Sran,A Nehra
4,Q de Kock,SV Samson,RR Pant,SW Billings,SK Raina,SE Marsh,WP Saha,YK Pathan,RV Uthappa,SA Yadav,Shakib Al Hasan,RG Sharma,TG Southee,SPD Smith,SS Tiwary,UT Khawaja,V Kohli,Sachin Baby,TM Head,STR Binny,SR Watson,S Dhawan,Yuvraj Singh
5,AJ Finch,DR Smith,BB McCullum,DJ Bravo,HM Amla,Gurkeerat Singh,GJ Maxwell,DA Miller,AR Patel,G Gambhir,CA Lynn,Harbhajan Singh,AT Rayudu,AM Rahane,F du Plessis,CH Gayle,D Wiese,DA Warner,EJG Morgan,DJ Hooda
6,R Dhawan,UT Yadav,R Bhatia,SN Khan
7,P Negi,MA Agarwal,RA Jadeja,P Kumar,MM Sharma,PP Chawla,R Sathish,M Morkel,MJ Guptill,PA Patel,R Vinay Kumar,LMP Simmons,R Ashwin,Parvez Rasool,KW Richardson,KS Williamson,MC Henriques,NV Ojha,KV Sharma
8,S Nadeem,S Kaushik,S Ladda,PJ Sangwan,Swapnil Singh,S Gopal,UBT Chand,SM Boland,RP Singh,VR Aaron,S Aravind,YS Chahal,Mandeep Singh,T Shamsi,TA Boult,Mustafizur Rahman
9,SS Iyer,Z Khan,NM Coulter-Nile,Mohammed Shami,PV Tambe,SB Jakati,NS Naik,MG Johnson,Sandeep Sharma,SP Narine,MR Marsh,PSP Handscomb
10,A Mishra,Anureet Singh,GB Hogg,JW Hastings,Kuldeep Yadav,AS Rajpoot,JD Unadkat,J Suchith,M Ashwin,A Zampa,I Sharma,DL Chahar,AB Dinda,HV Patel,AF Milne
