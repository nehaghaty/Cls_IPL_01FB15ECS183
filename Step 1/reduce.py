import ast

f = open("playerVSplayer.txt","r")

runs=[]
for x in f:
	y = x.strip()
	l = y.split(':')
	l[1] = ast.literal_eval(l[1])
	runs.append(l)

f.close()

final = {}

for x in runs:
	final[x[0]] = [0,0,0,0,0,0,0,0]

for x in runs:
	for y in range(0,8):
		final[x[0]][y] += x[1][y]

f = open("final.txt","w")

for x in final.keys():
	val = str(x)
	val += ","
	for y in range(0,7):
		val += str(final[x][y])
		val += ","
	val += str(final[x][7])
	print(val)
	f.write("%s\n" % val)

f.close()
