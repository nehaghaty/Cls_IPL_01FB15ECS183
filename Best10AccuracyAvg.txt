﻿Top 10 accurate matches

Match 56
	PREDICTED : 

	Team 1 Score  : 137
	Team 2 Score  : 139

	Winner :Team 2


	ACTUAL : 
	Team 1 138
	Team 2 139

	Accuracy: 99.63%
Match 1
	PREDICTED : 

	Team 1 Score  : 128
	Team 2 Score  : 130

	Winner :Team 2

	ACTUAL : 
	Team 1 121
	Team 2 126

	Accuracy: 95.72

Match 7
	PREDICTED : 

	Team 1 Score  : 119
	Team 2 Score  : 121

	Winner :Team 2


	ACTUAL : 
	Team 1 111
	Team 2 113

	Accuracy:92.85%


Match 16
	PREDICTED : 

	Team 1 Score  : 185
	Team 2 Score  : 145

	Winner :Team 1


	ACTUAL : 
	Team 1 185
	Team 2 172

	Accuracy: 92.15%

Match 20
	PREDICTED : 

	Team 1 Score  : 161
	Team 2 Score  : 158

	Winner :Team 1


	ACTUAL : 
	Team 1 160
	Team 2 162

	Accuracy: 98.56%

Match 28
	PREDICTED : 

	Team 1 Score  : 153
	Team 2 Score  : 137

	Winner :Team 1

	ACTUAL : 
	Team 1 154
	Team 2 131

	Accuracy: 97.38%
Match 34
	PREDICTED : 

	Team 1 Score  : 117
	Team 2 Score  : 119

	Winner :Team 2


	ACTUAL : 
	Team 1 126
	Team 2 129

	Accuracy: 92.54%

Match 51
	PREDICTED : 

	Team 1 Score  : 117
	Team 2 Score  : 118

	Winner :Team 2

	ACTUAL : 
	Team 1 124
	Team 2 125

	Accuracy:94.2%

Match 58
	PREDICTED : 

	Team 1 Score  : 161
	Team 2 Score  : 121

	Winner :Team 1

	ACTUAL : 
	Team 1 162
	Team 2 140

	Accuracy:92.90%

Match 59
	PREDICTED : 

	Team 1 Score  : 163
	Team 2 Score  : 159


	ACTUAL : 
	Team 1 162
	Team 2 163

	Accuracy: 98.43%

Average Accuracy for 10 best matches :  95.436 %
