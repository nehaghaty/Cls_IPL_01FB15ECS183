Match No. 59
Date: 2016-05-27
Gujarat Lions VS Sunrisers Hyderabad

Toss: 
Sunrisers Hyderabad won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 2	Run - 1
	Current Score - 1/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 1/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 4	Run - 1
	Current Score - 2/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 2/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 6	Run - 1
	Current Score - 3/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


Over: 2
	Ball - 1	Run - 0
	Current Score - 3/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


	Ball - 2	Run - 6
	Current Score - 9/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


	Ball - 3	Run - 1
	Current Score - 10/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : TA Boult


	Ball - 4	Run - 1
	Current Score - 11/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


	Ball - 5	Run - 1
	Current Score - 12/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : TA Boult


	Ball - 6	Run - 1
	Current Score - 13/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


Over: 3
	Ball - 1	Run - 0
	Current Score - 13/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 2	Run - 0
	Current Score - 13/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 13/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 13/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 13/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


	Ball - 6	Run - 4
	Current Score - 17/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : B Kumar


Over: 4
	Ball - 1	Run - 0
	Current Score - 17/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


	Ball - 2	Run - 0
	Current Score - 17/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


	Ball - 3	Run - 2
	Current Score - 19/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : TA Boult


	Ball - 4	Run - 1
	Current Score - 20/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : TA Boult


	Ball - 5	Run - 0
	Current Score - 20/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : TA Boult


	Ball - 6	Run - 0
	Current Score - 20/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : TA Boult


Over: 5
	Ball - 1	Run - 4
	Current Score - 24/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 2	Run - 1
	Current Score - 25/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : BB Sran


	Ball - 3	Run - 0
	Current Score - 25/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : BB Sran


	Ball - 4	Run - 1
	Current Score - 26/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 5	Run - 2
	Current Score - 28/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 6	Run - 1
	Current Score - 29/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : BB Sran


Over: 6
	Ball - 1	Run - 4
	Current Score - 33/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 2	Run - 0
	Current Score - 33/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 3	Run - 0
	Current Score - 33/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 4	Run - 1
	Current Score - 34/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : Bipul Sharma


	Ball - 5	Run - 0
	Current Score - 34/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : Bipul Sharma


	Ball - 6	Run - 0
	Current Score - 34/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : Bipul Sharma


Over: 7
	Ball - 1	Run - 0
	Current Score - 34/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BCJ Cutting


	Ball - 2	Run - 1
	Current Score - 35/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : BCJ Cutting


	Ball - 3	Run - 1
	Current Score - 36/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BCJ Cutting


	Ball - 4	Run - 0
	Current Score - 36/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BCJ Cutting


	Ball - 5	Run - 4
	Current Score - 40/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : BCJ Cutting


	Ball - 6	Run - 3
	Current Score - 43/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : BCJ Cutting


Over: 8
	Ball - 1	Run - 1
	Current Score - 44/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : MC Henriques


	Ball - 2	Run - 1
	Current Score - 45/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : MC Henriques


	Ball - 3	Run - 0
	Current Score - 45/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : MC Henriques


	Ball - 4	Run - 1
	Current Score - 46/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : MC Henriques


	Ball - 5	Run - 0
	Current Score - 46/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : MC Henriques


	Ball - 6	Run - 6
	Current Score - 52/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : MC Henriques


Over: 9
	Ball - 1	Run - 2
	Current Score - 54/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 2	Run - 4
	Current Score - 58/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 3	Run - 0
	Current Score - 58/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 4	Run - 4
	Current Score - 62/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : Bipul Sharma


	Ball - 5	Run - 1
	Current Score - 63/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : Bipul Sharma


	Ball - 6	Run - 4
	Current Score - 67/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : Bipul Sharma


Over: 10
	Ball - 1	Run - 4
	Current Score - 71/0
	Striker : ER Dwivedi
	Non Striker : BB McCullum
	Bowler : MC Henriques


	Ball - 2	Run - 1
	Current Score - 72/0
	Striker : BB McCullum
	Non Striker : ER Dwivedi
	Bowler : MC Henriques


	Ball - 3	Run - 0
	Current Score - 72/1
	Wicket!!!! 
	Striker : SK Raina
	Non Striker : ER Dwivedi
	Bowler : MC Henriques


	Ball - 4	Run - 1
	Current Score - 73/1
	Striker : ER Dwivedi
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 5	Run - 0
	Current Score - 73/2
	Wicket!!!! 
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 6	Run - 0
	Current Score - 73/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


Over: 11
	Ball - 1	Run - 1
	Current Score - 74/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 2	Run - 1
	Current Score - 75/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


	Ball - 3	Run - 6
	Current Score - 81/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


	Ball - 4	Run - 0
	Current Score - 81/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


	Ball - 5	Run - 0
	Current Score - 81/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


	Ball - 6	Run - 2
	Current Score - 83/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


Over: 12
	Ball - 1	Run - 4
	Current Score - 87/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 2	Run - 4
	Current Score - 91/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 3	Run - 0
	Current Score - 91/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 4	Run - 6
	Current Score - 97/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 5	Run - 4
	Current Score - 101/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 6	Run - 0
	Current Score - 101/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


Over: 13
	Ball - 1	Run - 1
	Current Score - 102/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


	Ball - 2	Run - 2
	Current Score - 104/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


	Ball - 3	Run - 0
	Current Score - 104/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


	Ball - 4	Run - 0
	Current Score - 104/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


	Ball - 5	Run - 2
	Current Score - 106/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


	Ball - 6	Run - 1
	Current Score - 107/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BCJ Cutting


Over: 14
	Ball - 1	Run - 0
	Current Score - 107/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 2	Run - 1
	Current Score - 108/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : MC Henriques


	Ball - 3	Run - 0
	Current Score - 108/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : MC Henriques


	Ball - 4	Run - 0
	Current Score - 108/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : MC Henriques


	Ball - 5	Run - 1
	Current Score - 109/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 6	Run - 4
	Current Score - 113/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


Over: 15
	Ball - 1	Run - 1
	Current Score - 114/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 2	Run - 0
	Current Score - 114/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 3	Run - 4
	Current Score - 118/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 4	Run - 1
	Current Score - 119/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


	Ball - 5	Run - 0
	Current Score - 119/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


	Ball - 6	Run - 6
	Current Score - 125/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BB Sran


Over: 16
	Ball - 1	Run - 1
	Current Score - 126/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : TA Boult


	Ball - 2	Run - 1
	Current Score - 127/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 3	Run - 4
	Current Score - 131/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 4	Run - 0
	Current Score - 131/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 5	Run - 4
	Current Score - 135/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 6	Run - 0
	Current Score - 135/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


Over: 17
	Ball - 1	Run - 0
	Current Score - 135/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : B Kumar


	Ball - 2	Run - 4
	Current Score - 139/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 140/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 140/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 5	Run - 1
	Current Score - 141/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 141/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : B Kumar


Over: 18
	Ball - 1	Run - 1
	Current Score - 142/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BCJ Cutting


	Ball - 2	Run - 0
	Current Score - 142/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BCJ Cutting


	Ball - 3	Run - 2
	Current Score - 144/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BCJ Cutting


	Ball - 4	Run - 4
	Current Score - 148/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : BCJ Cutting


	Ball - 5	Run - 1
	Current Score - 149/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


	Ball - 6	Run - 0
	Current Score - 149/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : BCJ Cutting


Over: 19
	Ball - 1	Run - 2
	Current Score - 151/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : TA Boult


	Ball - 2	Run - 1
	Current Score - 152/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 3	Run - 1
	Current Score - 153/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : TA Boult


	Ball - 4	Run - 1
	Current Score - 154/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 5	Run - 0
	Current Score - 154/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : TA Boult


	Ball - 6	Run - 1
	Current Score - 155/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : TA Boult


Over: 20
	Ball - 1	Run - 2
	Current Score - 157/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 2	Run - 1
	Current Score - 158/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 158/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : B Kumar


	Ball - 4	Run - 1
	Current Score - 159/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 5	Run - 4
	Current Score - 163/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 163/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : B Kumar


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 2	Run - 1
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 3	Run - 0
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 4	Run - 0
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 5	Run - 0
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 6	Run - 4
	Current Score - 5/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


Over: 2
	Ball - 1	Run - 0
	Current Score - 5/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 5/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 6/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


	Ball - 4	Run - 0
	Current Score - 6/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 6/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


	Ball - 6	Run - 2
	Current Score - 8/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


Over: 3
	Ball - 1	Run - 0
	Current Score - 8/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 2	Run - 0
	Current Score - 8/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 3	Run - 1
	Current Score - 9/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 4	Run - 1
	Current Score - 10/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 5	Run - 0
	Current Score - 10/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 6	Run - 4
	Current Score - 14/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


Over: 4
	Ball - 1	Run - 4
	Current Score - 18/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 18/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


	Ball - 3	Run - 2
	Current Score - 20/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


	Ball - 4	Run - 1
	Current Score - 21/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 21/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 6	Run - 1
	Current Score - 22/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DS Kulkarni


Over: 5
	Ball - 1	Run - 1
	Current Score - 23/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DR Smith


	Ball - 2	Run - 0
	Current Score - 23/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DR Smith


	Ball - 3	Run - 1
	Current Score - 24/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DR Smith


	Ball - 4	Run - 0
	Current Score - 24/1
	Wicket!!!! 
	Striker : MC Henriques
	Non Striker : S Dhawan
	Bowler : DR Smith


	Ball - 5	Run - 1
	Current Score - 25/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : DR Smith


	Ball - 6	Run - 1
	Current Score - 26/1
	Striker : MC Henriques
	Non Striker : S Dhawan
	Bowler : DR Smith


Over: 6
	Ball - 1	Run - 1
	Current Score - 27/1
	Striker : MC Henriques
	Non Striker : S Dhawan
	Bowler : SK Raina


	Ball - 2	Run - 1
	Current Score - 28/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : SK Raina


	Ball - 3	Run - 0
	Current Score - 28/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : SK Raina


	Ball - 4	Run - 6
	Current Score - 34/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : SK Raina


	Ball - 5	Run - 0
	Current Score - 34/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : SK Raina


	Ball - 6	Run - 1
	Current Score - 35/1
	Striker : MC Henriques
	Non Striker : S Dhawan
	Bowler : SK Raina


Over: 7
	Ball - 1	Run - 0
	Current Score - 35/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : S Kaushik


	Ball - 2	Run - 0
	Current Score - 35/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : S Kaushik


	Ball - 3	Run - 0
	Current Score - 35/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : S Kaushik


	Ball - 4	Run - 0
	Current Score - 35/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : S Kaushik


	Ball - 5	Run - 0
	Current Score - 35/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : S Kaushik


	Ball - 6	Run - 0
	Current Score - 35/1
	Striker : S Dhawan
	Non Striker : MC Henriques
	Bowler : S Kaushik


Over: 8
	Ball - 1	Run - 0
	Current Score - 35/2
	Wicket!!!! 
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : SK Raina


	Ball - 2	Run - 1
	Current Score - 36/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : SK Raina


	Ball - 3	Run - 1
	Current Score - 37/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : SK Raina


	Ball - 4	Run - 1
	Current Score - 38/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : SK Raina


	Ball - 5	Run - 1
	Current Score - 39/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : SK Raina


	Ball - 6	Run - 6
	Current Score - 45/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : SK Raina


Over: 9
	Ball - 1	Run - 1
	Current Score - 46/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 2	Run - 2
	Current Score - 48/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 3	Run - 0
	Current Score - 48/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 4	Run - 0
	Current Score - 48/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 5	Run - 6
	Current Score - 54/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 6	Run - 1
	Current Score - 55/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : S Kaushik


Over: 10
	Ball - 1	Run - 1
	Current Score - 56/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 2	Run - 4
	Current Score - 60/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 3	Run - 4
	Current Score - 64/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 4	Run - 0
	Current Score - 64/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 65/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 6	Run - 2
	Current Score - 67/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


Over: 11
	Ball - 1	Run - 1
	Current Score - 68/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 2	Run - 0
	Current Score - 68/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 3	Run - 1
	Current Score - 69/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : S Kaushik


	Ball - 4	Run - 1
	Current Score - 70/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 5	Run - 4
	Current Score - 74/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 6	Run - 4
	Current Score - 78/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


Over: 12
	Ball - 1	Run - 1
	Current Score - 79/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 79/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 3	Run - 2
	Current Score - 81/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 4	Run - 0
	Current Score - 81/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 82/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 6	Run - 1
	Current Score - 83/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


Over: 13
	Ball - 1	Run - 1
	Current Score - 84/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 2	Run - 2
	Current Score - 86/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 3	Run - 0
	Current Score - 86/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 4	Run - 6
	Current Score - 92/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 5	Run - 0
	Current Score - 92/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


	Ball - 6	Run - 0
	Current Score - 92/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : S Kaushik


Over: 14
	Ball - 1	Run - 4
	Current Score - 96/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 96/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 97/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 4	Run - 1
	Current Score - 98/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


	Ball - 5	Run - 4
	Current Score - 102/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


	Ball - 6	Run - 2
	Current Score - 104/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


Over: 15
	Ball - 1	Run - 4
	Current Score - 108/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DR Smith


	Ball - 2	Run - 1
	Current Score - 109/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DR Smith


	Ball - 3	Run - 1
	Current Score - 110/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DR Smith


	Ball - 4	Run - 4
	Current Score - 114/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DR Smith


	Ball - 5	Run - 0
	Current Score - 114/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DR Smith


	Ball - 6	Run - 1
	Current Score - 115/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DR Smith


Over: 16
	Ball - 1	Run - 1
	Current Score - 116/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 2	Run - 1
	Current Score - 117/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 118/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 119/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 5	Run - 0
	Current Score - 119/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 6	Run - 4
	Current Score - 123/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


Over: 17
	Ball - 1	Run - 1
	Current Score - 124/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 2	Run - 2
	Current Score - 126/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 3	Run - 4
	Current Score - 130/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 4	Run - 1
	Current Score - 131/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : P Kumar


	Ball - 5	Run - 1
	Current Score - 132/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 6	Run - 0
	Current Score - 132/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


Over: 18
	Ball - 1	Run - 0
	Current Score - 132/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 132/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 133/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 4	Run - 4
	Current Score - 137/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 137/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


	Ball - 6	Run - 6
	Current Score - 143/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DS Kulkarni


Over: 19
	Ball - 1	Run - 0
	Current Score - 143/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 2	Run - 2
	Current Score - 145/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 146/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 147/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 148/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 148/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : DJ Bravo


Over: 20
	Ball - 1	Run - 4
	Current Score - 152/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : P Kumar


	Ball - 2	Run - 0
	Current Score - 152/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : P Kumar


	Ball - 3	Run - 6
	Current Score - 158/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : P Kumar


	Ball - 4	Run - 1
	Current Score - 159/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 5	Run - 0
	Current Score - 159/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar

	Ball - 6	Run - 0
	Current Score - 159/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : P Kumar


PREDICTED : 

Team 1 Score  : 163
Team 2 Score  : 159


ACTUAL : 
Team 1 162
Team 2 163

Accuracy: 98.43%

Innings 1 AJ Finch-50
Innings 2 DA Warner-89
