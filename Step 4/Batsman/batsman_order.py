import sys
import os
import yaml

ifile = 980901

for j in range(60):
	ifile=str(ifile+j*2)+'.yaml'
	batsman1=[]
	batsman2=[]
	data = yaml.load(open(ifile,'r').read())


	innings1 = data['innings'][0]['1st innings']['deliveries']
	for row in innings1:
		x= row[row.keys()[0]]['batsman']
		if x not in batsman1:
			batsman1.append(x)




	innings2 = data['innings'][1]['2nd innings']['deliveries']
	for row in innings2:
		x= row[row.keys()[0]]['batsman']
		if x not in batsman2:
			batsman2.append(x)

	
	f=open('Match'+str(j+1) +'.txt','w')
	f.write('1st innings\n\n')
	for i in batsman1:
		f.write(i+'\n')
	f.write('\n2nd innings\n\n')
	for i in batsman2:
		f.write(i+'\n')
	ifile= 980901
