Match No. 19
Date: 2016-04-24
Gujarat Lions VS Royal Challengers Bangalore

Toss: 
Royal Challengers Bangalore won the toss and chose to bat

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 6
	Current Score - 6/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Kumar


	Ball - 2	Run - 0
	Current Score - 6/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Kumar


	Ball - 3	Run - 0
	Current Score - 6/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Kumar


	Ball - 4	Run - 1
	Current Score - 7/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 5	Run - 1
	Current Score - 8/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Kumar


	Ball - 6	Run - 0
	Current Score - 8/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Kumar


Over: 2
	Ball - 1	Run - 6
	Current Score - 14/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 2	Run - 1
	Current Score - 15/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : DS Kulkarni


	Ball - 3	Run - 0
	Current Score - 15/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : DS Kulkarni


	Ball - 4	Run - 0
	Current Score - 15/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : DS Kulkarni


	Ball - 5	Run - 2
	Current Score - 17/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : DS Kulkarni


	Ball - 6	Run - 0
	Current Score - 17/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : DS Kulkarni


Over: 3
	Ball - 1	Run - 0
	Current Score - 17/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 2	Run - 0
	Current Score - 17/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 3	Run - 0
	Current Score - 17/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 4	Run - 4
	Current Score - 21/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 5	Run - 1
	Current Score - 22/0
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Kumar


	Ball - 6	Run - 1
	Current Score - 23/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Kumar


Over: 4
	Ball - 1	Run - 1
	Current Score - 24/0
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 24/1
	Wicket!!!! 
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 25/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 4	Run - 2
	Current Score - 27/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 27/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 6	Run - 2
	Current Score - 29/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


Over: 5
	Ball - 1	Run - 0
	Current Score - 29/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : PV Tambe


	Ball - 2	Run - 0
	Current Score - 29/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : PV Tambe


	Ball - 3	Run - 0
	Current Score - 29/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : PV Tambe


	Ball - 4	Run - 1
	Current Score - 30/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : PV Tambe


	Ball - 5	Run - 0
	Current Score - 30/2
	Wicket!!!! 
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : PV Tambe


	Ball - 6	Run - 1
	Current Score - 31/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


Over: 6
	Ball - 1	Run - 4
	Current Score - 35/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 2	Run - 0
	Current Score - 35/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 3	Run - 1
	Current Score - 36/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 4	Run - 1
	Current Score - 37/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 5	Run - 0
	Current Score - 37/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 6	Run - 6
	Current Score - 43/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


Over: 7
	Ball - 1	Run - 0
	Current Score - 43/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 2	Run - 1
	Current Score - 44/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 3	Run - 2
	Current Score - 46/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 4	Run - 1
	Current Score - 47/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 5	Run - 1
	Current Score - 48/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 6	Run - 4
	Current Score - 52/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


Over: 8
	Ball - 1	Run - 1
	Current Score - 53/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : PV Tambe


	Ball - 2	Run - 1
	Current Score - 54/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 3	Run - 1
	Current Score - 55/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : PV Tambe


	Ball - 4	Run - 1
	Current Score - 56/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 5	Run - 0
	Current Score - 56/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 6	Run - 1
	Current Score - 57/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : PV Tambe


Over: 9
	Ball - 1	Run - 0
	Current Score - 57/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 2	Run - 1
	Current Score - 58/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 59/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 4	Run - 1
	Current Score - 60/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 5	Run - 0
	Current Score - 60/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 6	Run - 0
	Current Score - 60/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


Over: 10
	Ball - 1	Run - 6
	Current Score - 66/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 2	Run - 0
	Current Score - 66/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 3	Run - 1
	Current Score - 67/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : PV Tambe


	Ball - 4	Run - 1
	Current Score - 68/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 5	Run - 2
	Current Score - 70/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


	Ball - 6	Run - 0
	Current Score - 70/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : PV Tambe


Over: 11
	Ball - 1	Run - 0
	Current Score - 70/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 2	Run - 0
	Current Score - 70/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 71/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 4	Run - 1
	Current Score - 72/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 5	Run - 1
	Current Score - 73/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 6	Run - 0
	Current Score - 73/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : RA Jadeja


Over: 12
	Ball - 1	Run - 0
	Current Score - 73/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 2	Run - 0
	Current Score - 73/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 3	Run - 0
	Current Score - 73/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 4	Run - 1
	Current Score - 74/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 5	Run - 6
	Current Score - 80/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 6	Run - 6
	Current Score - 86/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


Over: 13
	Ball - 1	Run - 0
	Current Score - 86/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 86/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : DJ Bravo


	Ball - 3	Run - 4
	Current Score - 90/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : DJ Bravo


	Ball - 4	Run - 0
	Current Score - 90/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 91/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 91/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : DJ Bravo


Over: 14
	Ball - 1	Run - 1
	Current Score - 92/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 2	Run - 6
	Current Score - 98/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 3	Run - 0
	Current Score - 98/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 4	Run - 0
	Current Score - 98/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 5	Run - 1
	Current Score - 99/2
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 6	Run - 1
	Current Score - 100/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : SB Jakati


Over: 15
	Ball - 1	Run - 1
	Current Score - 101/2
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 101/3
	Wicket!!!! 
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 3	Run - 4
	Current Score - 105/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 4	Run - 6
	Current Score - 111/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 5	Run - 4
	Current Score - 115/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 6	Run - 4
	Current Score - 119/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DJ Bravo


Over: 16
	Ball - 1	Run - 1
	Current Score - 120/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DS Kulkarni


	Ball - 2	Run - 4
	Current Score - 124/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DS Kulkarni


	Ball - 3	Run - 4
	Current Score - 128/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DS Kulkarni


	Ball - 4	Run - 4
	Current Score - 132/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DS Kulkarni


	Ball - 5	Run - 6
	Current Score - 138/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DS Kulkarni


	Ball - 6	Run - 4
	Current Score - 142/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : DS Kulkarni


Over: 17
	Ball - 1	Run - 1
	Current Score - 143/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 2	Run - 2
	Current Score - 145/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 3	Run - 2
	Current Score - 147/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 4	Run - 2
	Current Score - 149/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 5	Run - 2
	Current Score - 151/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 6	Run - 0
	Current Score - 151/3
	Striker : SN Khan
	Non Striker : KL Rahul
	Bowler : P Kumar


Over: 18
	Ball - 1	Run - 2
	Current Score - 153/3
	Striker : KL Rahul
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 153/3
	Striker : KL Rahul
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 3	Run - 0
	Current Score - 153/4
	Wicket!!!! 
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 154/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DJ Bravo


	Ball - 5	Run - 4
	Current Score - 158/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DJ Bravo


	Ball - 6	Run - 4
	Current Score - 162/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DJ Bravo


Over: 19
	Ball - 1	Run - 1
	Current Score - 163/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DS Kulkarni


	Ball - 2	Run - 4
	Current Score - 167/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DS Kulkarni


	Ball - 3	Run - 6
	Current Score - 173/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DS Kulkarni


	Ball - 4	Run - 1
	Current Score - 174/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : DS Kulkarni


	Ball - 5	Run - 1
	Current Score - 175/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DS Kulkarni


	Ball - 6	Run - 4
	Current Score - 179/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : DS Kulkarni


Over: 20
	Ball - 1	Run - 0
	Current Score - 179/5
	Wicket!!!! 
	Striker : STR Binny
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 2	Run - 6
	Current Score - 185/5
	Striker : STR Binny
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 186/5
	Striker : SN Khan
	Non Striker : STR Binny
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 187/5
	Striker : STR Binny
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 5	Run - 2
	Current Score - 189/5
	Striker : STR Binny
	Non Striker : SN Khan
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 189/5
	Striker : STR Binny
	Non Striker : SN Khan
	Bowler : DJ Bravo


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : DR Smith
	Non Striker : BB McCullum
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 0/0
	Striker : DR Smith
	Non Striker : BB McCullum
	Bowler : YS Chahal


	Ball - 3	Run - 1
	Current Score - 1/0
	Striker : BB McCullum
	Non Striker : DR Smith
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 1/0
	Striker : BB McCullum
	Non Striker : DR Smith
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 2/0
	Striker : DR Smith
	Non Striker : BB McCullum
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 2/0
	Striker : DR Smith
	Non Striker : BB McCullum
	Bowler : YS Chahal


Over: 2
	Ball - 1	Run - 4
	Current Score - 6/0
	Striker : BB McCullum
	Non Striker : DR Smith
	Bowler : KW Richardson


	Ball - 2	Run - 0
	Current Score - 6/0
	Striker : BB McCullum
	Non Striker : DR Smith
	Bowler : KW Richardson


	Ball - 3	Run - 0
	Current Score - 6/0
	Striker : BB McCullum
	Non Striker : DR Smith
	Bowler : KW Richardson


	Ball - 4	Run - 0
	Current Score - 6/1
	Wicket!!!! 
	Striker : SK Raina
	Non Striker : DR Smith
	Bowler : KW Richardson


	Ball - 5	Run - 1
	Current Score - 7/1
	Striker : DR Smith
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 6	Run - 0
	Current Score - 7/2
	Wicket!!!! 
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : KW Richardson


Over: 3
	Ball - 1	Run - 1
	Current Score - 8/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 2	Run - 6
	Current Score - 14/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 3	Run - 1
	Current Score - 15/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : YS Chahal


	Ball - 4	Run - 2
	Current Score - 17/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : YS Chahal


	Ball - 5	Run - 0
	Current Score - 17/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 18/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : YS Chahal


Over: 4
	Ball - 1	Run - 0
	Current Score - 18/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 0
	Current Score - 18/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 1
	Current Score - 19/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 0
	Current Score - 19/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 1
	Current Score - 20/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 0
	Current Score - 20/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


Over: 5
	Ball - 1	Run - 1
	Current Score - 21/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 2	Run - 0
	Current Score - 21/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 22/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 4	Run - 1
	Current Score - 23/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 23/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 6	Run - 4
	Current Score - 27/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


Over: 6
	Ball - 1	Run - 4
	Current Score - 31/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 2	Run - 0
	Current Score - 31/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 3	Run - 2
	Current Score - 33/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 4	Run - 6
	Current Score - 39/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 5	Run - 1
	Current Score - 40/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : KW Richardson


	Ball - 6	Run - 2
	Current Score - 42/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : KW Richardson


Over: 7
	Ball - 1	Run - 0
	Current Score - 42/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 2	Run - 0
	Current Score - 42/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 3	Run - 1
	Current Score - 43/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : T Shamsi


	Ball - 4	Run - 1
	Current Score - 44/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 5	Run - 1
	Current Score - 45/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : T Shamsi


	Ball - 6	Run - 6
	Current Score - 51/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : T Shamsi


Over: 8
	Ball - 1	Run - 4
	Current Score - 55/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 4
	Current Score - 59/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 0
	Current Score - 59/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 4
	Current Score - 63/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 0
	Current Score - 63/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 1
	Current Score - 64/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


Over: 9
	Ball - 1	Run - 1
	Current Score - 65/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : T Shamsi


	Ball - 2	Run - 4
	Current Score - 69/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : T Shamsi


	Ball - 3	Run - 1
	Current Score - 70/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 4	Run - 0
	Current Score - 70/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 5	Run - 0
	Current Score - 70/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 6	Run - 4
	Current Score - 74/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


Over: 10
	Ball - 1	Run - 1
	Current Score - 75/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 2	Run - 4
	Current Score - 79/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 3	Run - 0
	Current Score - 79/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 4	Run - 0
	Current Score - 79/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 79/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 6	Run - 1
	Current Score - 80/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


Over: 11
	Ball - 1	Run - 1
	Current Score - 81/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : T Shamsi


	Ball - 2	Run - 1
	Current Score - 82/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 3	Run - 0
	Current Score - 82/3
	Wicket!!!! 
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 4	Run - 2
	Current Score - 84/3
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 5	Run - 1
	Current Score - 85/3
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : T Shamsi


	Ball - 6	Run - 1
	Current Score - 86/3
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : T Shamsi


Over: 12
	Ball - 1	Run - 0
	Current Score - 86/3
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : KW Richardson


	Ball - 2	Run - 1
	Current Score - 87/3
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 3	Run - 0
	Current Score - 87/3
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 4	Run - 2
	Current Score - 89/3
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 5	Run - 0
	Current Score - 89/4
	Wicket!!!! 
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : KW Richardson


	Ball - 6	Run - 6
	Current Score - 95/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : KW Richardson


Over: 13
	Ball - 1	Run - 1
	Current Score - 96/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 96/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 3	Run - 0
	Current Score - 96/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 4	Run - 4
	Current Score - 100/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 101/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 101/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : YS Chahal


Over: 14
	Ball - 1	Run - 1
	Current Score - 102/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : SR Watson


	Ball - 2	Run - 4
	Current Score - 106/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : SR Watson


	Ball - 3	Run - 4
	Current Score - 110/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : SR Watson


	Ball - 4	Run - 4
	Current Score - 114/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : SR Watson


	Ball - 5	Run - 1
	Current Score - 115/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 6	Run - 6
	Current Score - 121/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : SR Watson


Over: 15
	Ball - 1	Run - 2
	Current Score - 123/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : T Shamsi


	Ball - 2	Run - 1
	Current Score - 124/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : T Shamsi


	Ball - 3	Run - 1
	Current Score - 125/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : T Shamsi


	Ball - 4	Run - 0
	Current Score - 125/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : T Shamsi


	Ball - 5	Run - 0
	Current Score - 125/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : T Shamsi


	Ball - 6	Run - 2
	Current Score - 127/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : T Shamsi


Over: 16
	Ball - 1	Run - 0
	Current Score - 127/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 127/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : YS Chahal


	Ball - 3	Run - 1
	Current Score - 128/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 128/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 5	Run - 2
	Current Score - 130/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 130/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : YS Chahal


Over: 17
	Ball - 1	Run - 6
	Current Score - 136/4
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 1
	Current Score - 137/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 0
	Current Score - 137/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 0
	Current Score - 137/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 0
	Current Score - 137/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 4
	Current Score - 141/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


Over: 18
	Ball - 1	Run - 1
	Current Score - 142/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : KW Richardson


	Ball - 2	Run - 0
	Current Score - 142/4
	Striker : SK Raina
	Non Striker : DJ Bravo
	Bowler : KW Richardson


	Ball - 3	Run - 0
	Current Score - 142/5
	Wicket!!!! 
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : KW Richardson


	Ball - 4	Run - 1
	Current Score - 143/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : KW Richardson


	Ball - 5	Run - 1
	Current Score - 144/5
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : KW Richardson


	Ball - 6	Run - 0
	Current Score - 144/5
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : KW Richardson


Over: 19
	Ball - 1	Run - 1
	Current Score - 145/5
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 0
	Current Score - 145/5
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 1
	Current Score - 146/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 0
	Current Score - 146/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 1
	Current Score - 147/5
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 1
	Current Score - 148/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


Over: 20
	Ball - 1	Run - 1
	Current Score - 149/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 150/5
	Striker : AJ Finch
	Non Striker : DJ Bravo
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 151/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : SR Watson


	Ball - 4	Run - 4
	Current Score - 155/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 155/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 155/5
	Striker : DJ Bravo
	Non Striker : AJ Finch
	Bowler : SR Watson


PREDICTED : 

Team 1 Score  : 189
Team 2 Score  : 155

Winner :Team 1

Team 1
SN Khan-72
AB de Villiers-39
KL Rahul-38
V Kohli-15
SR Watson-13
STR Binny-9
KM Jadhav-3


Team 2
SK Raina-54
KD Karthik-49
DJ Bravo-36
BB McCullum-5
AJ Finch-5
RA Jadeja-5
DR Smith-1

ACTUAL : 
Team 1 180
Team 2 182

Innings 1 V Kohli-100
Innings 2 KD Karthik-50
