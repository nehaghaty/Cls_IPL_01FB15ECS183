Match No. 57
Date: 2016-05-24
Gujarat Lions VS Royal Challengers Bangalore

Toss: 
Royal Challengers Bangalore won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 1
	Current Score - 1/0
	Striker : BB McCullum
	Non Striker : AJ Finch
	Bowler : S Aravind


	Ball - 2	Run - 0
	Current Score - 1/1
	Wicket!!!! 
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : S Aravind


	Ball - 3	Run - 2
	Current Score - 3/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : S Aravind


	Ball - 4	Run - 1
	Current Score - 4/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : S Aravind


	Ball - 5	Run - 4
	Current Score - 8/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : S Aravind


	Ball - 6	Run - 0
	Current Score - 8/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : S Aravind


Over: 2
	Ball - 1	Run - 0
	Current Score - 8/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 4
	Current Score - 12/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 4
	Current Score - 16/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 0
	Current Score - 16/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 0
	Current Score - 16/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 1
	Current Score - 17/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


Over: 3
	Ball - 1	Run - 1
	Current Score - 18/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : S Aravind


	Ball - 2	Run - 4
	Current Score - 22/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : S Aravind


	Ball - 3	Run - 0
	Current Score - 22/1
	Striker : AJ Finch
	Non Striker : KD Karthik
	Bowler : S Aravind


	Ball - 4	Run - 1
	Current Score - 23/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : S Aravind


	Ball - 5	Run - 0
	Current Score - 23/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : S Aravind


	Ball - 6	Run - 0
	Current Score - 23/1
	Striker : KD Karthik
	Non Striker : AJ Finch
	Bowler : S Aravind


Over: 4
	Ball - 1	Run - 0
	Current Score - 23/2
	Wicket!!!! 
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 2	Run - 0
	Current Score - 23/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 24/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 4	Run - 1
	Current Score - 25/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 5	Run - 1
	Current Score - 26/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 26/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : SR Watson


Over: 5
	Ball - 1	Run - 0
	Current Score - 26/3
	Wicket!!!! 
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 1
	Current Score - 27/3
	Striker : KD Karthik
	Non Striker : DR Smith
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 4
	Current Score - 31/3
	Striker : KD Karthik
	Non Striker : DR Smith
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 0
	Current Score - 31/3
	Striker : KD Karthik
	Non Striker : DR Smith
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 0
	Current Score - 31/3
	Striker : KD Karthik
	Non Striker : DR Smith
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 1
	Current Score - 32/3
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : Iqbal Abdulla


Over: 6
	Ball - 1	Run - 1
	Current Score - 33/3
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 2	Run - 0
	Current Score - 33/3
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 3	Run - 0
	Current Score - 33/3
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 4	Run - 4
	Current Score - 37/3
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 37/3
	Striker : DR Smith
	Non Striker : KD Karthik
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 37/4
	Wicket!!!! 
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : SR Watson


Over: 7
	Ball - 1	Run - 2
	Current Score - 39/4
	Striker : KD Karthik
	Non Striker : RA Jadeja
	Bowler : CJ Jordan


	Ball - 2	Run - 6
	Current Score - 45/4
	Striker : KD Karthik
	Non Striker : RA Jadeja
	Bowler : CJ Jordan


	Ball - 3	Run - 1
	Current Score - 46/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : CJ Jordan


	Ball - 4	Run - 4
	Current Score - 50/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : CJ Jordan


	Ball - 5	Run - 0
	Current Score - 50/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : CJ Jordan


	Ball - 6	Run - 0
	Current Score - 50/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : CJ Jordan


Over: 8
	Ball - 1	Run - 0
	Current Score - 50/4
	Striker : KD Karthik
	Non Striker : RA Jadeja
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 50/4
	Striker : KD Karthik
	Non Striker : RA Jadeja
	Bowler : YS Chahal


	Ball - 3	Run - 1
	Current Score - 51/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : YS Chahal


	Ball - 4	Run - 4
	Current Score - 55/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 56/4
	Striker : KD Karthik
	Non Striker : RA Jadeja
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 57/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : YS Chahal


Over: 9
	Ball - 1	Run - 1
	Current Score - 58/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : STR Binny


	Ball - 2	Run - 4
	Current Score - 62/4
	Striker : RA Jadeja
	Non Striker : KD Karthik
	Bowler : STR Binny


	Ball - 3	Run - 1
	Current Score - 63/4
	Striker : KD Karthik
	Non Striker : RA Jadeja
	Bowler : STR Binny


	Ball - 4	Run - 0
	Current Score - 63/5
	Wicket!!!! 
	Striker : DJ Bravo
	Non Striker : RA Jadeja
	Bowler : STR Binny


	Ball - 5	Run - 4
	Current Score - 67/5
	Striker : DJ Bravo
	Non Striker : RA Jadeja
	Bowler : STR Binny


	Ball - 6	Run - 0
	Current Score - 67/5
	Striker : DJ Bravo
	Non Striker : RA Jadeja
	Bowler : STR Binny


Over: 10
	Ball - 1	Run - 1
	Current Score - 68/5
	Striker : DJ Bravo
	Non Striker : RA Jadeja
	Bowler : YS Chahal


	Ball - 2	Run - 1
	Current Score - 69/5
	Striker : RA Jadeja
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 3	Run - 0
	Current Score - 69/5
	Striker : RA Jadeja
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 69/6
	Wicket!!!! 
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 70/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 70/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : YS Chahal


Over: 11
	Ball - 1	Run - 0
	Current Score - 70/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 4
	Current Score - 74/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 4
	Current Score - 78/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 2
	Current Score - 80/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 1
	Current Score - 81/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 0
	Current Score - 81/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : Iqbal Abdulla


Over: 12
	Ball - 1	Run - 4
	Current Score - 85/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 2	Run - 2
	Current Score - 87/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 3	Run - 0
	Current Score - 87/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 87/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 5	Run - 2
	Current Score - 89/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 89/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : YS Chahal


Over: 13
	Ball - 1	Run - 1
	Current Score - 90/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 2	Run - 6
	Current Score - 96/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


	Ball - 3	Run - 1
	Current Score - 97/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : Iqbal Abdulla


	Ball - 4	Run - 0
	Current Score - 97/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : Iqbal Abdulla


	Ball - 5	Run - 4
	Current Score - 101/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : Iqbal Abdulla


	Ball - 6	Run - 1
	Current Score - 102/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : Iqbal Abdulla


Over: 14
	Ball - 1	Run - 4
	Current Score - 106/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : CJ Jordan


	Ball - 2	Run - 6
	Current Score - 112/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : CJ Jordan


	Ball - 3	Run - 1
	Current Score - 113/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : CJ Jordan


	Ball - 4	Run - 0
	Current Score - 113/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : CJ Jordan


	Ball - 5	Run - 4
	Current Score - 117/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : CJ Jordan


	Ball - 6	Run - 6
	Current Score - 123/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : CJ Jordan


Over: 15
	Ball - 1	Run - 1
	Current Score - 124/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : S Aravind


	Ball - 2	Run - 0
	Current Score - 124/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : S Aravind


	Ball - 3	Run - 0
	Current Score - 124/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : S Aravind


	Ball - 4	Run - 6
	Current Score - 130/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : S Aravind


	Ball - 5	Run - 1
	Current Score - 131/6
	Striker : DJ Bravo
	Non Striker : ER Dwivedi
	Bowler : S Aravind


	Ball - 6	Run - 1
	Current Score - 132/6
	Striker : ER Dwivedi
	Non Striker : DJ Bravo
	Bowler : S Aravind


Over: 16
	Ball - 1	Run - 0
	Current Score - 132/7
	Wicket!!!! 
	Striker : P Kumar
	Non Striker : ER Dwivedi
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 133/7
	Striker : ER Dwivedi
	Non Striker : P Kumar
	Bowler : SR Watson


	Ball - 3	Run - 4
	Current Score - 137/7
	Striker : ER Dwivedi
	Non Striker : P Kumar
	Bowler : SR Watson


	Ball - 4	Run - 1
	Current Score - 138/7
	Striker : P Kumar
	Non Striker : ER Dwivedi
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 138/7
	Striker : P Kumar
	Non Striker : ER Dwivedi
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 138/7
	Striker : P Kumar
	Non Striker : ER Dwivedi
	Bowler : SR Watson


Over: 17
	Ball - 1	Run - 6
	Current Score - 144/7
	Striker : ER Dwivedi
	Non Striker : P Kumar
	Bowler : YS Chahal


	Ball - 2	Run - 4
	Current Score - 148/7
	Striker : ER Dwivedi
	Non Striker : P Kumar
	Bowler : YS Chahal


	Ball - 3	Run - 0
	Current Score - 148/8
	Wicket!!!! 
	Striker : DS Kulkarni
	Non Striker : P Kumar
	Bowler : YS Chahal


	Ball - 4	Run - 4
	Current Score - 152/8
	Striker : DS Kulkarni
	Non Striker : P Kumar
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 153/8
	Striker : P Kumar
	Non Striker : DS Kulkarni
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 154/8
	Striker : DS Kulkarni
	Non Striker : P Kumar
	Bowler : YS Chahal


Over: 18
	Ball - 1	Run - 0
	Current Score - 154/8
	Striker : P Kumar
	Non Striker : DS Kulkarni
	Bowler : CJ Jordan


	Ball - 2	Run - 0
	Current Score - 154/8
	Striker : P Kumar
	Non Striker : DS Kulkarni
	Bowler : CJ Jordan


	Ball - 3	Run - 0
	Current Score - 154/8
	Striker : P Kumar
	Non Striker : DS Kulkarni
	Bowler : CJ Jordan


	Ball - 4	Run - 0
	Current Score - 154/8
	Striker : P Kumar
	Non Striker : DS Kulkarni
	Bowler : CJ Jordan


	Ball - 5	Run - 0
	Current Score - 154/8
	Striker : P Kumar
	Non Striker : DS Kulkarni
	Bowler : CJ Jordan


	Ball - 6	Run - 0
	Current Score - 154/9
	Wicket!!!! 
	Striker : SB Jakati
	Non Striker : DS Kulkarni
	Bowler : CJ Jordan


Over: 19
	Ball - 1	Run - 0
	Current Score - 154/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : SR Watson


	Ball - 2	Run - 4
	Current Score - 158/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : SR Watson


	Ball - 3	Run - 0
	Current Score - 158/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : SR Watson


	Ball - 4	Run - 1
	Current Score - 159/9
	Striker : SB Jakati
	Non Striker : DS Kulkarni
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 159/9
	Striker : SB Jakati
	Non Striker : DS Kulkarni
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 159/9
	Striker : SB Jakati
	Non Striker : DS Kulkarni
	Bowler : SR Watson


Over: 20
	Ball - 1	Run - 0
	Current Score - 159/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : CJ Jordan


	Ball - 2	Run - 2
	Current Score - 161/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : CJ Jordan


	Ball - 3	Run - 6
	Current Score - 167/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : CJ Jordan


	Ball - 4	Run - 4
	Current Score - 171/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : CJ Jordan


	Ball - 5	Run - 0
	Current Score - 171/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : CJ Jordan


	Ball - 6	Run - 2
	Current Score - 173/9
	Striker : DS Kulkarni
	Non Striker : SB Jakati
	Bowler : CJ Jordan


################################## Innings 2 ######################################

Over: 1
nf:
	Ball - 1	Run - 2
	Current Score - 2/0
	Striker : 
	Non Striker : CH Gayle
	Bowler : P Kumar


nf:
	Ball - 2	Run - 0
	Current Score - 2/1
	Wicket!!!! 
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : P Kumar


	Ball - 3	Run - 3
	Current Score - 5/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 4	Run - 1
	Current Score - 6/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : P Kumar


	Ball - 5	Run - 0
	Current Score - 6/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : P Kumar


	Ball - 6	Run - 0
	Current Score - 6/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : P Kumar


Over: 2
	Ball - 1	Run - 4
	Current Score - 10/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 2	Run - 1
	Current Score - 11/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 12/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 4	Run - 1
	Current Score - 13/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : DS Kulkarni


	Ball - 5	Run - 1
	Current Score - 14/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 6	Run - 1
	Current Score - 15/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : DS Kulkarni


Over: 3
	Ball - 1	Run - 0
	Current Score - 15/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 2	Run - 1
	Current Score - 16/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : P Kumar


	Ball - 3	Run - 1
	Current Score - 17/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 4	Run - 0
	Current Score - 17/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 5	Run - 4
	Current Score - 21/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 6	Run - 6
	Current Score - 27/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : P Kumar


Over: 4
	Ball - 1	Run - 1
	Current Score - 28/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 28/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 29/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : DS Kulkarni


	Ball - 4	Run - 0
	Current Score - 29/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : DS Kulkarni


	Ball - 5	Run - 1
	Current Score - 30/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 6	Run - 1
	Current Score - 31/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : DS Kulkarni


Over: 5
	Ball - 1	Run - 0
	Current Score - 31/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 2	Run - 1
	Current Score - 32/1
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 33/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 4	Run - 2
	Current Score - 35/1
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 5	Run - 0
	Current Score - 35/2
	Wicket!!!! 
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 6	Run - 1
	Current Score - 36/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


Over: 6
	Ball - 1	Run - 0
	Current Score - 36/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 2	Run - 1
	Current Score - 37/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 38/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 4	Run - 6
	Current Score - 44/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 44/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 6	Run - 0
	Current Score - 44/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


Over: 7
	Ball - 1	Run - 1
	Current Score - 45/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 2	Run - 1
	Current Score - 46/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 3	Run - 4
	Current Score - 50/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 4	Run - 1
	Current Score - 51/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 5	Run - 4
	Current Score - 55/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 6	Run - 1
	Current Score - 56/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


Over: 8
	Ball - 1	Run - 6
	Current Score - 62/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 62/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


	Ball - 3	Run - 1
	Current Score - 63/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 4	Run - 4
	Current Score - 67/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 67/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : DS Kulkarni


	Ball - 6	Run - 1
	Current Score - 68/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DS Kulkarni


Over: 9
	Ball - 1	Run - 1
	Current Score - 69/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 2	Run - 4
	Current Score - 73/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 3	Run - 4
	Current Score - 77/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 4	Run - 1
	Current Score - 78/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 5	Run - 1
	Current Score - 79/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 6	Run - 1
	Current Score - 80/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : SB Jakati


Over: 10
	Ball - 1	Run - 0
	Current Score - 80/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 2	Run - 4
	Current Score - 84/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 85/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 4	Run - 0
	Current Score - 85/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 5	Run - 0
	Current Score - 85/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : RA Jadeja


	Ball - 6	Run - 1
	Current Score - 86/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : RA Jadeja


Over: 11
	Ball - 1	Run - 1
	Current Score - 87/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 2	Run - 1
	Current Score - 88/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 3	Run - 1
	Current Score - 89/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 4	Run - 1
	Current Score - 90/2
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : SB Jakati


	Ball - 5	Run - 1
	Current Score - 91/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 6	Run - 6
	Current Score - 97/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : SB Jakati


Over: 12
	Ball - 1	Run - 1
	Current Score - 98/2
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 98/3
	Wicket!!!! 
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DJ Bravo


	Ball - 3	Run - 0
	Current Score - 98/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DJ Bravo


	Ball - 4	Run - 6
	Current Score - 104/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 105/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 6	Run - 1
	Current Score - 106/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DJ Bravo


Over: 13
	Ball - 1	Run - 4
	Current Score - 110/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 2	Run - 0
	Current Score - 110/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 111/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 4	Run - 2
	Current Score - 113/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 5	Run - 0
	Current Score - 113/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RA Jadeja


	Ball - 6	Run - 1
	Current Score - 114/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RA Jadeja


Over: 14
	Ball - 1	Run - 0
	Current Score - 114/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DJ Bravo


	Ball - 2	Run - 1
	Current Score - 115/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 116/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 117/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 5	Run - 0
	Current Score - 117/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 117/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DJ Bravo


Over: 15
	Ball - 1	Run - 0
	Current Score - 117/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : DR Smith


	Ball - 2	Run - 1
	Current Score - 118/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DR Smith


	Ball - 3	Run - 0
	Current Score - 118/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DR Smith


	Ball - 4	Run - 0
	Current Score - 118/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DR Smith


	Ball - 5	Run - 4
	Current Score - 122/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DR Smith


	Ball - 6	Run - 0
	Current Score - 122/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : DR Smith


Over: 16
	Ball - 1	Run - 1
	Current Score - 123/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 2	Run - 1
	Current Score - 124/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 3	Run - 0
	Current Score - 124/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : SB Jakati


	Ball - 4	Run - 1
	Current Score - 125/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 5	Run - 6
	Current Score - 131/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : SB Jakati


	Ball - 6	Run - 1
	Current Score - 132/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : SB Jakati


Over: 17
	Ball - 1	Run - 0
	Current Score - 132/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 2	Run - 6
	Current Score - 138/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 3	Run - 1
	Current Score - 139/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 4	Run - 0
	Current Score - 139/3
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : P Kumar


	Ball - 5	Run - 1
	Current Score - 140/3
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 6	Run - 0
	Current Score - 140/4
	Wicket!!!! 
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : P Kumar


Over: 18
	Ball - 1	Run - 2
	Current Score - 142/4
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 142/4
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 143/4
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 144/4
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : DJ Bravo


	Ball - 5	Run - 0
	Current Score - 144/4
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 144/4
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : DJ Bravo


Over: 19
	Ball - 1	Run - 0
	Current Score - 144/5
	Wicket!!!! 
	Striker : Sachin Baby
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 2	Run - 0
	Current Score - 144/5
	Striker : Sachin Baby
	Non Striker : KL Rahul
	Bowler : P Kumar


	Ball - 3	Run - 1
	Current Score - 145/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : P Kumar


	Ball - 4	Run - 0
	Current Score - 145/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : P Kumar


	Ball - 5	Run - 0
	Current Score - 145/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : P Kumar


	Ball - 6	Run - 1
	Current Score - 146/5
	Striker : Sachin Baby
	Non Striker : KL Rahul
	Bowler : P Kumar


Over: 20
	Ball - 1	Run - 0
	Current Score - 146/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 146/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : DJ Bravo


	Ball - 3	Run - 4
	Current Score - 150/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : DJ Bravo


	Ball - 4	Run - 2
	Current Score - 152/5
	Striker : KL Rahul
	Non Striker : Sachin Baby
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 153/5
	Striker : Sachin Baby
	Non Striker : KL Rahul
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 153/5
	Striker : Sachin Baby
	Non Striker : KL Rahul
	Bowler : DJ Bravo


PREDICTED : 

Team 1 Score  : 173
Team 2 Score  : 153

Winner :Team 1

Team 1
ER Dwivedi-59
KD Karthik-32
DJ Bravo-24
DS Kulkarni-24
RA Jadeja-15
AJ Finch-10
DR Smith-5
SK Raina-2
P Kumar-2
SB Jakati-0
BB McCullum-0


Team 2
V Kohli-54
AB de Villiers-44
KL Rahul-27
CH Gayle-24
-2
SR Watson-1
Sachin Baby-1

ACTUAL : 
Team 1 158
Team 2 159

Innings 1 DR Smith-73
Innings 2 AB de Villiers-79
