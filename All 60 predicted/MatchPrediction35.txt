Match No. 35
Date: 2016-05-07
Royal Challengers Bangalore VS Rising Pune Supergiants

Toss: 
Royal Challengers Bangalore won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 1
	Current Score - 1/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : STR Binny


	Ball - 2	Run - 1
	Current Score - 2/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : STR Binny


	Ball - 3	Run - 0
	Current Score - 2/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : STR Binny


	Ball - 4	Run - 1
	Current Score - 3/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : STR Binny


	Ball - 5	Run - 1
	Current Score - 4/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : STR Binny


	Ball - 6	Run - 0
	Current Score - 4/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : STR Binny


Over: 2
	Ball - 1	Run - 0
	Current Score - 4/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 2	Run - 1
	Current Score - 5/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


	Ball - 3	Run - 0
	Current Score - 5/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


	Ball - 4	Run - 4
	Current Score - 9/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


	Ball - 5	Run - 1
	Current Score - 10/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 6	Run - 1
	Current Score - 11/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


Over: 3
	Ball - 1	Run - 1
	Current Score - 12/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : CJ Jordan


	Ball - 2	Run - 0
	Current Score - 12/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : CJ Jordan


	Ball - 3	Run - 0
	Current Score - 12/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : CJ Jordan


	Ball - 4	Run - 4
	Current Score - 16/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : CJ Jordan


	Ball - 5	Run - 2
	Current Score - 18/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : CJ Jordan


	Ball - 6	Run - 4
	Current Score - 22/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : CJ Jordan


Over: 4
	Ball - 1	Run - 1
	Current Score - 23/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 24/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 25/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : SR Watson


	Ball - 4	Run - 0
	Current Score - 25/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 25/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 25/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : SR Watson


Over: 5
	Ball - 1	Run - 4
	Current Score - 29/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 2	Run - 1
	Current Score - 30/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


	Ball - 3	Run - 1
	Current Score - 31/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 4	Run - 6
	Current Score - 37/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 5	Run - 1
	Current Score - 38/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


	Ball - 6	Run - 2
	Current Score - 40/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : Parvez Rasool


Over: 6
	Ball - 1	Run - 4
	Current Score - 44/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 2	Run - 0
	Current Score - 44/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 3	Run - 4
	Current Score - 48/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 4	Run - 0
	Current Score - 48/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 5	Run - 1
	Current Score - 49/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : VR Aaron


	Ball - 6	Run - 0
	Current Score - 49/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : VR Aaron


Over: 7
	Ball - 1	Run - 0
	Current Score - 49/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 49/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 3	Run - 2
	Current Score - 51/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 4	Run - 1
	Current Score - 52/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 53/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 53/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : YS Chahal


Over: 8
	Ball - 1	Run - 0
	Current Score - 53/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : VR Aaron


	Ball - 2	Run - 1
	Current Score - 54/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 3	Run - 0
	Current Score - 54/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 4	Run - 1
	Current Score - 55/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : VR Aaron


	Ball - 5	Run - 1
	Current Score - 56/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


	Ball - 6	Run - 4
	Current Score - 60/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : VR Aaron


Over: 9
	Ball - 1	Run - 0
	Current Score - 60/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 60/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : YS Chahal


	Ball - 3	Run - 4
	Current Score - 64/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 64/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 65/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 66/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : YS Chahal


Over: 10
	Ball - 1	Run - 1
	Current Score - 67/0
	Striker : AM Rahane
	Non Striker : UT Khawaja
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 68/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 3	Run - 0
	Current Score - 68/0
	Striker : UT Khawaja
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 4	Run - 0
	Current Score - 68/1
	Wicket!!!! 
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 5	Run - 1
	Current Score - 69/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 69/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


Over: 11
	Ball - 1	Run - 4
	Current Score - 73/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : STR Binny


	Ball - 2	Run - 0
	Current Score - 73/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : STR Binny


	Ball - 3	Run - 1
	Current Score - 74/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : STR Binny


	Ball - 4	Run - 4
	Current Score - 78/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : STR Binny


	Ball - 5	Run - 1
	Current Score - 79/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : STR Binny


	Ball - 6	Run - 1
	Current Score - 80/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : STR Binny


Over: 12
	Ball - 1	Run - 0
	Current Score - 80/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 2	Run - 0
	Current Score - 80/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 3	Run - 1
	Current Score - 81/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : Parvez Rasool


	Ball - 4	Run - 1
	Current Score - 82/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


	Ball - 5	Run - 1
	Current Score - 83/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : Parvez Rasool


	Ball - 6	Run - 1
	Current Score - 84/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : Parvez Rasool


Over: 13
	Ball - 1	Run - 4
	Current Score - 88/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : VR Aaron


	Ball - 2	Run - 4
	Current Score - 92/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : VR Aaron


	Ball - 3	Run - 0
	Current Score - 92/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : VR Aaron


	Ball - 4	Run - 0
	Current Score - 92/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : VR Aaron


	Ball - 5	Run - 0
	Current Score - 92/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : VR Aaron


	Ball - 6	Run - 1
	Current Score - 93/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : VR Aaron


Over: 14
	Ball - 1	Run - 6
	Current Score - 99/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


	Ball - 2	Run - 0
	Current Score - 99/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


	Ball - 3	Run - 1
	Current Score - 100/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 4	Run - 0
	Current Score - 100/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 5	Run - 0
	Current Score - 100/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 6	Run - 1
	Current Score - 101/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


Over: 15
	Ball - 1	Run - 6
	Current Score - 107/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 2	Run - 2
	Current Score - 109/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 3	Run - 1
	Current Score - 110/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 110/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 111/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 6	Run - 0
	Current Score - 111/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : YS Chahal


Over: 16
	Ball - 1	Run - 0
	Current Score - 111/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 112/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 113/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


	Ball - 4	Run - 0
	Current Score - 113/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


	Ball - 5	Run - 1
	Current Score - 114/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 6	Run - 1
	Current Score - 115/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


Over: 17
	Ball - 1	Run - 1
	Current Score - 116/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


	Ball - 2	Run - 1
	Current Score - 117/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : YS Chahal


	Ball - 3	Run - 1
	Current Score - 118/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


	Ball - 4	Run - 2
	Current Score - 120/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


	Ball - 5	Run - 0
	Current Score - 120/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


	Ball - 6	Run - 4
	Current Score - 124/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : YS Chahal


Over: 18
	Ball - 1	Run - 4
	Current Score - 128/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 2	Run - 1
	Current Score - 129/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


	Ball - 3	Run - 4
	Current Score - 133/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


	Ball - 4	Run - 1
	Current Score - 134/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 5	Run - 0
	Current Score - 134/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 6	Run - 1
	Current Score - 135/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


Over: 19
	Ball - 1	Run - 0
	Current Score - 135/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 136/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 137/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 4	Run - 4
	Current Score - 141/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 141/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : SR Watson


	Ball - 6	Run - 1
	Current Score - 142/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : SR Watson


Over: 20
	Ball - 1	Run - 1
	Current Score - 143/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


	Ball - 2	Run - 1
	Current Score - 144/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 3	Run - 0
	Current Score - 144/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 4	Run - 0
	Current Score - 144/1
	Striker : SS Tiwary
	Non Striker : AM Rahane
	Bowler : CJ Jordan


	Ball - 5	Run - 1
	Current Score - 145/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


	Ball - 6	Run - 0
	Current Score - 145/1
	Striker : AM Rahane
	Non Striker : SS Tiwary
	Bowler : CJ Jordan


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 2
	Current Score - 2/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : AB Dinda


	Ball - 2	Run - 1
	Current Score - 3/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 3	Run - 0
	Current Score - 3/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 4	Run - 1
	Current Score - 4/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : AB Dinda


	Ball - 5	Run - 1
	Current Score - 5/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 6	Run - 0
	Current Score - 5/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


Over: 2
	Ball - 1	Run - 1
	Current Score - 6/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RP Singh


	Ball - 2	Run - 0
	Current Score - 6/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RP Singh


	Ball - 3	Run - 1
	Current Score - 7/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RP Singh


	Ball - 4	Run - 4
	Current Score - 11/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RP Singh


	Ball - 5	Run - 1
	Current Score - 12/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RP Singh


	Ball - 6	Run - 1
	Current Score - 13/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RP Singh


Over: 3
	Ball - 1	Run - 1
	Current Score - 14/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : AB Dinda


	Ball - 2	Run - 0
	Current Score - 14/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : AB Dinda


	Ball - 3	Run - 1
	Current Score - 15/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 4	Run - 0
	Current Score - 15/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 5	Run - 0
	Current Score - 15/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 6	Run - 0
	Current Score - 15/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


Over: 4
	Ball - 1	Run - 4
	Current Score - 19/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RP Singh


	Ball - 2	Run - 1
	Current Score - 20/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RP Singh


	Ball - 3	Run - 1
	Current Score - 21/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RP Singh


	Ball - 4	Run - 1
	Current Score - 22/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RP Singh


	Ball - 5	Run - 4
	Current Score - 26/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : RP Singh


	Ball - 6	Run - 1
	Current Score - 27/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : RP Singh


Over: 5
	Ball - 1	Run - 0
	Current Score - 27/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 2	Run - 0
	Current Score - 27/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 3	Run - 4
	Current Score - 31/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 4	Run - 2
	Current Score - 33/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : AB Dinda


	Ball - 5	Run - 1
	Current Score - 34/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : AB Dinda


	Ball - 6	Run - 6
	Current Score - 40/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : AB Dinda


Over: 6
	Ball - 1	Run - 2
	Current Score - 42/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : NLTC Perera


	Ball - 2	Run - 1
	Current Score - 43/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 3	Run - 1
	Current Score - 44/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : NLTC Perera


	Ball - 4	Run - 1
	Current Score - 45/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 5	Run - 0
	Current Score - 45/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 6	Run - 1
	Current Score - 46/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : NLTC Perera


Over: 7
	Ball - 1	Run - 4
	Current Score - 50/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 2	Run - 1
	Current Score - 51/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : A Zampa


	Ball - 3	Run - 0
	Current Score - 51/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : A Zampa


	Ball - 4	Run - 1
	Current Score - 52/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 5	Run - 0
	Current Score - 52/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 6	Run - 4
	Current Score - 56/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : A Zampa


Over: 8
	Ball - 1	Run - 1
	Current Score - 57/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : R Bhatia


	Ball - 2	Run - 1
	Current Score - 58/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : R Bhatia


	Ball - 3	Run - 0
	Current Score - 58/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : R Bhatia


	Ball - 4	Run - 1
	Current Score - 59/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : R Bhatia


	Ball - 5	Run - 1
	Current Score - 60/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : R Bhatia


	Ball - 6	Run - 0
	Current Score - 60/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : R Bhatia


Over: 9
	Ball - 1	Run - 1
	Current Score - 61/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : A Zampa


	Ball - 2	Run - 2
	Current Score - 63/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : A Zampa


	Ball - 3	Run - 1
	Current Score - 64/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 4	Run - 1
	Current Score - 65/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : A Zampa


	Ball - 5	Run - 1
	Current Score - 66/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 6	Run - 1
	Current Score - 67/0
	Striker : KL Rahul
	Non Striker : V Kohli
	Bowler : A Zampa


Over: 10
	Ball - 1	Run - 6
	Current Score - 73/0
	Striker : V Kohli
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 2	Run - 0
	Current Score - 73/1
	Wicket!!!! 
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 3	Run - 0
	Current Score - 73/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 4	Run - 1
	Current Score - 74/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : NLTC Perera


	Ball - 5	Run - 0
	Current Score - 74/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : NLTC Perera


	Ball - 6	Run - 4
	Current Score - 78/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : NLTC Perera


Over: 11
	Ball - 1	Run - 1
	Current Score - 79/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 2	Run - 0
	Current Score - 79/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 3	Run - 4
	Current Score - 83/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 4	Run - 0
	Current Score - 83/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 5	Run - 2
	Current Score - 85/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 6	Run - 4
	Current Score - 89/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


Over: 12
	Ball - 1	Run - 0
	Current Score - 89/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 2	Run - 0
	Current Score - 89/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 3	Run - 1
	Current Score - 90/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : A Zampa


	Ball - 4	Run - 1
	Current Score - 91/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 5	Run - 0
	Current Score - 91/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : A Zampa


	Ball - 6	Run - 0
	Current Score - 91/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : A Zampa


Over: 13
	Ball - 1	Run - 4
	Current Score - 95/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 2	Run - 4
	Current Score - 99/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 3	Run - 0
	Current Score - 99/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 4	Run - 0
	Current Score - 99/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 5	Run - 0
	Current Score - 99/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


	Ball - 6	Run - 0
	Current Score - 99/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : R Bhatia


Over: 14
	Ball - 1	Run - 0
	Current Score - 99/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 2	Run - 1
	Current Score - 100/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : NLTC Perera


	Ball - 3	Run - 4
	Current Score - 104/1
	Striker : KL Rahul
	Non Striker : AB de Villiers
	Bowler : NLTC Perera


	Ball - 4	Run - 1
	Current Score - 105/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 5	Run - 0
	Current Score - 105/1
	Striker : AB de Villiers
	Non Striker : KL Rahul
	Bowler : NLTC Perera


	Ball - 6	Run - 0
	Current Score - 105/2
	Wicket!!!! 
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : NLTC Perera


Over: 15
	Ball - 1	Run - 1
	Current Score - 106/2
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : R Bhatia


	Ball - 2	Run - 0
	Current Score - 106/2
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : R Bhatia


	Ball - 3	Run - 0
	Current Score - 106/2
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : R Bhatia


	Ball - 4	Run - 1
	Current Score - 107/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : R Bhatia


	Ball - 5	Run - 0
	Current Score - 107/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : R Bhatia


	Ball - 6	Run - 1
	Current Score - 108/2
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : R Bhatia


Over: 16
	Ball - 1	Run - 0
	Current Score - 108/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 2	Run - 0
	Current Score - 108/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 3	Run - 1
	Current Score - 109/2
	Striker : SR Watson
	Non Striker : KL Rahul
	Bowler : RP Singh


	Ball - 4	Run - 1
	Current Score - 110/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 5	Run - 4
	Current Score - 114/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 6	Run - 0
	Current Score - 114/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : RP Singh


Over: 17
	Ball - 1	Run - 1
	Current Score - 115/2
	Striker : KL Rahul
	Non Striker : SR Watson
	Bowler : R Ashwin


	Ball - 2	Run - 0
	Current Score - 115/3
	Wicket!!!! 
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : R Ashwin


	Ball - 3	Run - 0
	Current Score - 115/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : R Ashwin


	Ball - 4	Run - 1
	Current Score - 116/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : R Ashwin


	Ball - 5	Run - 0
	Current Score - 116/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : R Ashwin


	Ball - 6	Run - 0
	Current Score - 116/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : R Ashwin


Over: 18
	Ball - 1	Run - 0
	Current Score - 116/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : A Zampa


	Ball - 2	Run - 0
	Current Score - 116/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : A Zampa


	Ball - 3	Run - 0
	Current Score - 116/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : A Zampa


	Ball - 4	Run - 1
	Current Score - 117/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : A Zampa


	Ball - 5	Run - 0
	Current Score - 117/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : A Zampa


	Ball - 6	Run - 1
	Current Score - 118/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : A Zampa


Over: 19
	Ball - 1	Run - 1
	Current Score - 119/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 2	Run - 1
	Current Score - 120/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : RP Singh


	Ball - 3	Run - 1
	Current Score - 121/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 4	Run - 0
	Current Score - 121/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 5	Run - 0
	Current Score - 121/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : RP Singh


	Ball - 6	Run - 0
	Current Score - 121/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : RP Singh


Over: 20
	Ball - 1	Run - 0
	Current Score - 121/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : AB Dinda


	Ball - 2	Run - 1
	Current Score - 122/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : AB Dinda


	Ball - 3	Run - 1
	Current Score - 123/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : AB Dinda


	Ball - 4	Run - 4
	Current Score - 127/3
	Striker : SR Watson
	Non Striker : TM Head
	Bowler : AB Dinda


	Ball - 5	Run - 1
	Current Score - 128/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : AB Dinda


	Ball - 6	Run - 0
	Current Score - 128/3
	Striker : TM Head
	Non Striker : SR Watson
	Bowler : AB Dinda


PREDICTED : 

Team 1 Score  : 145
Team 2 Score  : 128

Winner :Team 1

Team 1
AM Rahane-70
UT Khawaja-38
SS Tiwary-37


Team 2
KL Rahul-63
V Kohli-45
SR Watson-12
AB de Villiers-4
TM Head-4

ACTUAL : 
Team 1 191
Team 2 195

Innings 1 AM Rahane-74
Innings 2 V Kohli-106
