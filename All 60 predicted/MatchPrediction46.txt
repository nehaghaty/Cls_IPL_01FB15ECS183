Match No. 46
Date: 2016-05-15
Kings XI Punjab VS Sunrisers Hyderabad

Toss: 
Kings XI Punjab won the toss and chose to bat

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/1
	Wicket!!!! 
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


	Ball - 2	Run - 1
	Current Score - 1/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 1/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : B Kumar


	Ball - 4	Run - 2
	Current Score - 3/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : B Kumar


	Ball - 5	Run - 1
	Current Score - 4/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 4/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


Over: 2
	Ball - 1	Run - 4
	Current Score - 8/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : A Nehra


	Ball - 2	Run - 0
	Current Score - 8/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : A Nehra


	Ball - 3	Run - 1
	Current Score - 9/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : A Nehra


	Ball - 4	Run - 1
	Current Score - 10/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : A Nehra


	Ball - 5	Run - 1
	Current Score - 11/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : A Nehra


	Ball - 6	Run - 0
	Current Score - 11/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : A Nehra


Over: 3
	Ball - 1	Run - 2
	Current Score - 13/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : B Kumar


	Ball - 2	Run - 1
	Current Score - 14/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 15/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : B Kumar


	Ball - 4	Run - 1
	Current Score - 16/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


	Ball - 5	Run - 2
	Current Score - 18/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


	Ball - 6	Run - 6
	Current Score - 24/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : B Kumar


Over: 4
	Ball - 1	Run - 1
	Current Score - 25/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : A Nehra


	Ball - 2	Run - 1
	Current Score - 26/1
	Striker : M Vijay
	Non Striker : WP Saha
	Bowler : A Nehra


	Ball - 3	Run - 1
	Current Score - 27/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : A Nehra


	Ball - 4	Run - 6
	Current Score - 33/1
	Striker : WP Saha
	Non Striker : M Vijay
	Bowler : A Nehra


	Ball - 5	Run - 0
	Current Score - 33/2
	Wicket!!!! 
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : A Nehra


	Ball - 6	Run - 1
	Current Score - 34/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : A Nehra


Over: 5
	Ball - 1	Run - 1
	Current Score - 35/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 35/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 35/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 1
	Current Score - 36/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 6
	Current Score - 42/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 42/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : Mustafizur Rahman


Over: 6
	Ball - 1	Run - 3
	Current Score - 45/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : BCJ Cutting


	Ball - 2	Run - 4
	Current Score - 49/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : BCJ Cutting


	Ball - 3	Run - 1
	Current Score - 50/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : BCJ Cutting


	Ball - 4	Run - 0
	Current Score - 50/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : BCJ Cutting


	Ball - 5	Run - 0
	Current Score - 50/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : BCJ Cutting


	Ball - 6	Run - 6
	Current Score - 56/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : BCJ Cutting


Over: 7
	Ball - 1	Run - 0
	Current Score - 56/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : KV Sharma


	Ball - 2	Run - 0
	Current Score - 56/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : KV Sharma


	Ball - 3	Run - 0
	Current Score - 56/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : KV Sharma


	Ball - 4	Run - 1
	Current Score - 57/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 5	Run - 0
	Current Score - 57/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 6	Run - 1
	Current Score - 58/2
	Striker : Gurkeerat Singh
	Non Striker : M Vijay
	Bowler : KV Sharma


Over: 8
	Ball - 1	Run - 0
	Current Score - 58/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 2	Run - 0
	Current Score - 58/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 3	Run - 2
	Current Score - 60/2
	Striker : M Vijay
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 4	Run - 0
	Current Score - 60/3
	Wicket!!!! 
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 5	Run - 6
	Current Score - 66/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 6	Run - 0
	Current Score - 66/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


Over: 9
	Ball - 1	Run - 6
	Current Score - 72/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 2	Run - 1
	Current Score - 73/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 3	Run - 1
	Current Score - 74/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 4	Run - 1
	Current Score - 75/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 5	Run - 1
	Current Score - 76/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 6	Run - 1
	Current Score - 77/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


Over: 10
	Ball - 1	Run - 1
	Current Score - 78/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Yuvraj Singh


	Ball - 2	Run - 1
	Current Score - 79/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Yuvraj Singh


	Ball - 3	Run - 1
	Current Score - 80/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Yuvraj Singh


	Ball - 4	Run - 2
	Current Score - 82/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Yuvraj Singh


	Ball - 5	Run - 4
	Current Score - 86/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Yuvraj Singh


	Ball - 6	Run - 6
	Current Score - 92/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Yuvraj Singh


Over: 11
	Ball - 1	Run - 4
	Current Score - 96/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 2	Run - 1
	Current Score - 97/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 3	Run - 2
	Current Score - 99/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 4	Run - 1
	Current Score - 100/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 5	Run - 0
	Current Score - 100/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 6	Run - 4
	Current Score - 104/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


Over: 12
	Ball - 1	Run - 1
	Current Score - 105/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 2	Run - 4
	Current Score - 109/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 3	Run - 0
	Current Score - 109/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 4	Run - 0
	Current Score - 109/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 5	Run - 0
	Current Score - 109/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 6	Run - 1
	Current Score - 110/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


Over: 13
	Ball - 1	Run - 6
	Current Score - 116/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : KV Sharma


	Ball - 2	Run - 1
	Current Score - 117/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 3	Run - 6
	Current Score - 123/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 4	Run - 0
	Current Score - 123/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 5	Run - 0
	Current Score - 123/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


	Ball - 6	Run - 4
	Current Score - 127/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : KV Sharma


Over: 14
	Ball - 1	Run - 0
	Current Score - 127/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : A Nehra


	Ball - 2	Run - 2
	Current Score - 129/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : A Nehra


	Ball - 3	Run - 0
	Current Score - 129/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : A Nehra


	Ball - 4	Run - 1
	Current Score - 130/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : A Nehra


	Ball - 5	Run - 1
	Current Score - 131/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : A Nehra


	Ball - 6	Run - 6
	Current Score - 137/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : A Nehra


Over: 15
	Ball - 1	Run - 0
	Current Score - 137/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 2
	Current Score - 139/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 139/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 1
	Current Score - 140/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 4
	Current Score - 144/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 1
	Current Score - 145/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


Over: 16
	Ball - 1	Run - 0
	Current Score - 145/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 2	Run - 1
	Current Score - 146/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 3	Run - 0
	Current Score - 146/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 4	Run - 1
	Current Score - 147/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


	Ball - 5	Run - 1
	Current Score - 148/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : MC Henriques


	Ball - 6	Run - 1
	Current Score - 149/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : MC Henriques


Over: 17
	Ball - 1	Run - 1
	Current Score - 150/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 150/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 1
	Current Score - 151/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 4
	Current Score - 155/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 0
	Current Score - 155/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 6
	Current Score - 161/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


Over: 18
	Ball - 1	Run - 1
	Current Score - 162/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : B Kumar


	Ball - 2	Run - 0
	Current Score - 162/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 163/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : B Kumar


	Ball - 4	Run - 4
	Current Score - 167/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 167/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 167/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : B Kumar


Over: 19
	Ball - 1	Run - 0
	Current Score - 167/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 4
	Current Score - 171/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 1
	Current Score - 172/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 4
	Current Score - 176/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 0
	Current Score - 176/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 176/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : Mustafizur Rahman


Over: 20
	Ball - 1	Run - 0
	Current Score - 176/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : B Kumar


	Ball - 2	Run - 2
	Current Score - 178/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 179/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 179/3
	Striker : Gurkeerat Singh
	Non Striker : DA Miller
	Bowler : B Kumar


	Ball - 5	Run - 1
	Current Score - 180/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 180/3
	Striker : DA Miller
	Non Striker : Gurkeerat Singh
	Bowler : B Kumar


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 1
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : Sandeep Sharma


	Ball - 2	Run - 1
	Current Score - 2/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : Sandeep Sharma


	Ball - 3	Run - 1
	Current Score - 3/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : Sandeep Sharma


	Ball - 4	Run - 1
	Current Score - 4/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : Sandeep Sharma


	Ball - 5	Run - 4
	Current Score - 8/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : Sandeep Sharma


	Ball - 6	Run - 0
	Current Score - 8/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : Sandeep Sharma


Over: 2
	Ball - 1	Run - 0
	Current Score - 8/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : M Vijay


	Ball - 2	Run - 6
	Current Score - 14/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : M Vijay


	Ball - 3	Run - 2
	Current Score - 16/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : M Vijay


	Ball - 4	Run - 1
	Current Score - 17/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : M Vijay


	Ball - 5	Run - 0
	Current Score - 17/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : M Vijay


	Ball - 6	Run - 3
	Current Score - 20/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : M Vijay


Over: 3
	Ball - 1	Run - 0
	Current Score - 20/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : MM Sharma


	Ball - 2	Run - 0
	Current Score - 20/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : MM Sharma


	Ball - 3	Run - 4
	Current Score - 24/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : MM Sharma


	Ball - 4	Run - 0
	Current Score - 24/1
	Wicket!!!! 
	Striker : DJ Hooda
	Non Striker : S Dhawan
	Bowler : MM Sharma


	Ball - 5	Run - 0
	Current Score - 24/1
	Striker : DJ Hooda
	Non Striker : S Dhawan
	Bowler : MM Sharma


	Ball - 6	Run - 1
	Current Score - 25/1
	Striker : S Dhawan
	Non Striker : DJ Hooda
	Bowler : MM Sharma


Over: 4
	Ball - 1	Run - 1
	Current Score - 26/1
	Striker : S Dhawan
	Non Striker : DJ Hooda
	Bowler : Anureet Singh


	Ball - 2	Run - 0
	Current Score - 26/1
	Striker : S Dhawan
	Non Striker : DJ Hooda
	Bowler : Anureet Singh


	Ball - 3	Run - 1
	Current Score - 27/1
	Striker : DJ Hooda
	Non Striker : S Dhawan
	Bowler : Anureet Singh


	Ball - 4	Run - 0
	Current Score - 27/1
	Striker : DJ Hooda
	Non Striker : S Dhawan
	Bowler : Anureet Singh


	Ball - 5	Run - 0
	Current Score - 27/2
	Wicket!!!! 
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : Anureet Singh


	Ball - 6	Run - 1
	Current Score - 28/2
	Striker : S Dhawan
	Non Striker : Yuvraj Singh
	Bowler : Anureet Singh


Over: 5
	Ball - 1	Run - 0
	Current Score - 28/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : MP Stoinis


	Ball - 2	Run - 0
	Current Score - 28/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : MP Stoinis


	Ball - 3	Run - 2
	Current Score - 30/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : MP Stoinis


	Ball - 4	Run - 0
	Current Score - 30/2
	Striker : Yuvraj Singh
	Non Striker : S Dhawan
	Bowler : MP Stoinis


	Ball - 5	Run - 0
	Current Score - 30/3
	Wicket!!!! 
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : MP Stoinis


	Ball - 6	Run - 1
	Current Score - 31/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : MP Stoinis


Over: 6
	Ball - 1	Run - 3
	Current Score - 34/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : GJ Maxwell


	Ball - 2	Run - 0
	Current Score - 34/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : GJ Maxwell


	Ball - 3	Run - 2
	Current Score - 36/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : GJ Maxwell


	Ball - 4	Run - 0
	Current Score - 36/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : GJ Maxwell


	Ball - 5	Run - 1
	Current Score - 37/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : GJ Maxwell


	Ball - 6	Run - 3
	Current Score - 40/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : GJ Maxwell


Over: 7
	Ball - 1	Run - 0
	Current Score - 40/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 2	Run - 2
	Current Score - 42/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 3	Run - 1
	Current Score - 43/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : AR Patel


	Ball - 4	Run - 1
	Current Score - 44/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 5	Run - 0
	Current Score - 44/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 6	Run - 1
	Current Score - 45/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : AR Patel


Over: 8
	Ball - 1	Run - 0
	Current Score - 45/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : Anureet Singh


	Ball - 2	Run - 0
	Current Score - 45/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : Anureet Singh


	Ball - 3	Run - 1
	Current Score - 46/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : Anureet Singh


	Ball - 4	Run - 0
	Current Score - 46/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : Anureet Singh


	Ball - 5	Run - 1
	Current Score - 47/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : Anureet Singh


	Ball - 6	Run - 0
	Current Score - 47/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : Anureet Singh


Over: 9
	Ball - 1	Run - 1
	Current Score - 48/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 2	Run - 4
	Current Score - 52/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 3	Run - 0
	Current Score - 52/3
	Striker : BCJ Cutting
	Non Striker : S Dhawan
	Bowler : AR Patel


	Ball - 4	Run - 1
	Current Score - 53/3
	Striker : S Dhawan
	Non Striker : BCJ Cutting
	Bowler : AR Patel


	Ball - 5	Run - 0
	Current Score - 53/4
	Wicket!!!! 
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : AR Patel


	Ball - 6	Run - 1
	Current Score - 54/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : AR Patel


Over: 10
	Ball - 1	Run - 0
	Current Score - 54/4
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : MP Stoinis


	Ball - 2	Run - 1
	Current Score - 55/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : MP Stoinis


	Ball - 3	Run - 1
	Current Score - 56/4
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : MP Stoinis


	Ball - 4	Run - 0
	Current Score - 56/4
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : MP Stoinis


	Ball - 5	Run - 4
	Current Score - 60/4
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : MP Stoinis


	Ball - 6	Run - 1
	Current Score - 61/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : MP Stoinis


Over: 11
	Ball - 1	Run - 1
	Current Score - 62/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


	Ball - 2	Run - 0
	Current Score - 62/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


	Ball - 3	Run - 0
	Current Score - 62/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


	Ball - 4	Run - 2
	Current Score - 64/4
	Striker : BCJ Cutting
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


	Ball - 5	Run - 1
	Current Score - 65/4
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : Sandeep Sharma


	Ball - 6	Run - 0
	Current Score - 65/4
	Striker : MC Henriques
	Non Striker : BCJ Cutting
	Bowler : Sandeep Sharma


Over: 12
	Ball - 1	Run - 0
	Current Score - 65/5
	Wicket!!!! 
	Striker : NV Ojha
	Non Striker : MC Henriques
	Bowler : Anureet Singh


	Ball - 2	Run - 0
	Current Score - 65/6
	Wicket!!!! 
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : Anureet Singh


	Ball - 3	Run - 0
	Current Score - 65/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : Anureet Singh


	Ball - 4	Run - 1
	Current Score - 66/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : Anureet Singh


	Ball - 5	Run - 0
	Current Score - 66/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : Anureet Singh


	Ball - 6	Run - 1
	Current Score - 67/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : Anureet Singh


Over: 13
	Ball - 1	Run - 1
	Current Score - 68/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : AR Patel


	Ball - 2	Run - 2
	Current Score - 70/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : AR Patel


	Ball - 3	Run - 1
	Current Score - 71/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : AR Patel


	Ball - 4	Run - 0
	Current Score - 71/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : AR Patel


	Ball - 5	Run - 0
	Current Score - 71/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : AR Patel


	Ball - 6	Run - 1
	Current Score - 72/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : AR Patel


Over: 14
	Ball - 1	Run - 0
	Current Score - 72/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : MM Sharma


	Ball - 2	Run - 1
	Current Score - 73/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : MM Sharma


	Ball - 3	Run - 0
	Current Score - 73/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : MM Sharma


	Ball - 4	Run - 1
	Current Score - 74/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : MM Sharma


	Ball - 5	Run - 0
	Current Score - 74/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : MM Sharma


	Ball - 6	Run - 4
	Current Score - 78/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : MM Sharma


Over: 15
	Ball - 1	Run - 0
	Current Score - 78/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : MP Stoinis


	Ball - 2	Run - 0
	Current Score - 78/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : MP Stoinis


	Ball - 3	Run - 1
	Current Score - 79/6
	Striker : MC Henriques
	Non Striker : KV Sharma
	Bowler : MP Stoinis


	Ball - 4	Run - 1
	Current Score - 80/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : MP Stoinis


	Ball - 5	Run - 0
	Current Score - 80/7
	Wicket!!!! 
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : MP Stoinis


	Ball - 6	Run - 1
	Current Score - 81/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : MP Stoinis


Over: 16
	Ball - 1	Run - 4
	Current Score - 85/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


	Ball - 2	Run - 6
	Current Score - 91/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


	Ball - 3	Run - 1
	Current Score - 92/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : Sandeep Sharma


	Ball - 4	Run - 0
	Current Score - 92/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : Sandeep Sharma


	Ball - 5	Run - 0
	Current Score - 92/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : Sandeep Sharma


	Ball - 6	Run - 1
	Current Score - 93/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : Sandeep Sharma


Over: 17
	Ball - 1	Run - 4
	Current Score - 97/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : AR Patel


	Ball - 2	Run - 2
	Current Score - 99/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : AR Patel


	Ball - 3	Run - 6
	Current Score - 105/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : AR Patel


	Ball - 4	Run - 1
	Current Score - 106/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : AR Patel


	Ball - 5	Run - 0
	Current Score - 106/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : AR Patel


	Ball - 6	Run - 4
	Current Score - 110/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : AR Patel


Over: 18
	Ball - 1	Run - 0
	Current Score - 110/8
	Wicket!!!! 
	Striker : A Nehra
	Non Striker : B Kumar
	Bowler : MM Sharma


	Ball - 2	Run - 1
	Current Score - 111/8
	Striker : B Kumar
	Non Striker : A Nehra
	Bowler : MM Sharma


	Ball - 3	Run - 0
	Current Score - 111/9
	Wicket!!!! 
	Striker : Mustafizur Rahman
	Non Striker : A Nehra
	Bowler : MM Sharma


	Ball - 4	Run - 0
	Current Score - 111/9
	Striker : Mustafizur Rahman
	Non Striker : A Nehra
	Bowler : MM Sharma


	Ball - 5	Run - 0
	Current Score - 111/10

	All Out!PREDICTED : 

Team 1 Score  : 180
Team 2 Score  : 111

Winner :Team 1

Team 1
Gurkeerat Singh-73
DA Miller-61
M Vijay-28
WP Saha-18
HM Amla-0


Team 2
MC Henriques-31
BCJ Cutting-21
S Dhawan-18
B Kumar-16
DA Warner-13
KV Sharma-6
Yuvraj Singh-3
DJ Hooda-2
A Nehra-1
Mustafizur Rahman-0
NV Ojha-0

ACTUAL : 
Team 1 179
Team 2 180

Innings 1 HM Amla-96
Innings 2 DA Warner-52
