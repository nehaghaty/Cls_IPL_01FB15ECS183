Match No. 26
Date: 2016-04-30
Delhi Daredevils VS Kolkata Knight Riders

Toss: 
Kolkata Knight Riders won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 1
	Current Score - 1/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : AD Russell


	Ball - 2	Run - 0
	Current Score - 1/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : AD Russell


	Ball - 3	Run - 1
	Current Score - 2/0
	Striker : Q de Kock
	Non Striker : SS Iyer
	Bowler : AD Russell


	Ball - 4	Run - 1
	Current Score - 3/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : AD Russell


	Ball - 5	Run - 0
	Current Score - 3/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : AD Russell


	Ball - 6	Run - 2
	Current Score - 5/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : AD Russell


Over: 2
	Ball - 1	Run - 1
	Current Score - 6/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 2	Run - 1
	Current Score - 7/0
	Striker : Q de Kock
	Non Striker : SS Iyer
	Bowler : JO Holder


	Ball - 3	Run - 0
	Current Score - 7/0
	Striker : Q de Kock
	Non Striker : SS Iyer
	Bowler : JO Holder


	Ball - 4	Run - 0
	Current Score - 7/0
	Striker : Q de Kock
	Non Striker : SS Iyer
	Bowler : JO Holder


	Ball - 5	Run - 1
	Current Score - 8/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 6	Run - 0
	Current Score - 8/1
	Wicket!!!! 
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : JO Holder


Over: 3
	Ball - 1	Run - 0
	Current Score - 8/1
	Striker : Q de Kock
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 2	Run - 2
	Current Score - 10/1
	Striker : Q de Kock
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 3	Run - 0
	Current Score - 10/1
	Striker : Q de Kock
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 4	Run - 4
	Current Score - 14/1
	Striker : Q de Kock
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 5	Run - 1
	Current Score - 15/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : AD Russell


	Ball - 6	Run - 0
	Current Score - 15/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : AD Russell


Over: 4
	Ball - 1	Run - 1
	Current Score - 16/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 2	Run - 0
	Current Score - 16/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 3	Run - 0
	Current Score - 16/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 4	Run - 4
	Current Score - 20/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 5	Run - 6
	Current Score - 26/1
	Striker : SV Samson
	Non Striker : Q de Kock
	Bowler : JO Holder


	Ball - 6	Run - 1
	Current Score - 27/1
	Striker : Q de Kock
	Non Striker : SV Samson
	Bowler : JO Holder


Over: 5
	Ball - 1	Run - 1
	Current Score - 28/1
	Striker : Q de Kock
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 2	Run - 0
	Current Score - 28/2
	Wicket!!!! 
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 3	Run - 1
	Current Score - 29/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 4	Run - 4
	Current Score - 33/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 5	Run - 1
	Current Score - 34/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 6	Run - 4
	Current Score - 38/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


Over: 6
	Ball - 1	Run - 0
	Current Score - 38/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : AD Russell


	Ball - 2	Run - 1
	Current Score - 39/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 3	Run - 6
	Current Score - 45/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 4	Run - 1
	Current Score - 46/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : AD Russell


	Ball - 5	Run - 0
	Current Score - 46/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : AD Russell


	Ball - 6	Run - 1
	Current Score - 47/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


Over: 7
	Ball - 1	Run - 1
	Current Score - 48/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 2	Run - 0
	Current Score - 48/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 3	Run - 1
	Current Score - 49/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 4	Run - 0
	Current Score - 49/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 5	Run - 0
	Current Score - 49/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 6	Run - 1
	Current Score - 50/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


Over: 8
	Ball - 1	Run - 1
	Current Score - 51/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 2	Run - 0
	Current Score - 51/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 3	Run - 0
	Current Score - 51/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 4	Run - 6
	Current Score - 57/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 5	Run - 1
	Current Score - 58/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : UT Yadav


	Ball - 6	Run - 4
	Current Score - 62/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : UT Yadav


Over: 9
	Ball - 1	Run - 0
	Current Score - 62/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 2	Run - 1
	Current Score - 63/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 3	Run - 2
	Current Score - 65/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 4	Run - 6
	Current Score - 71/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 5	Run - 1
	Current Score - 72/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 6	Run - 6
	Current Score - 78/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


Over: 10
	Ball - 1	Run - 1
	Current Score - 79/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : PP Chawla


	Ball - 2	Run - 2
	Current Score - 81/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : PP Chawla


	Ball - 3	Run - 1
	Current Score - 82/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : PP Chawla


	Ball - 4	Run - 0
	Current Score - 82/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : PP Chawla


	Ball - 5	Run - 1
	Current Score - 83/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : PP Chawla


	Ball - 6	Run - 4
	Current Score - 87/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : PP Chawla


Over: 11
	Ball - 1	Run - 0
	Current Score - 87/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 2	Run - 0
	Current Score - 87/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 3	Run - 1
	Current Score - 88/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 4	Run - 1
	Current Score - 89/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 5	Run - 1
	Current Score - 90/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 6	Run - 1
	Current Score - 91/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


Over: 12
	Ball - 1	Run - 0
	Current Score - 91/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : PP Chawla


	Ball - 2	Run - 1
	Current Score - 92/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : PP Chawla


	Ball - 3	Run - 1
	Current Score - 93/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : PP Chawla


	Ball - 4	Run - 1
	Current Score - 94/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : PP Chawla


	Ball - 5	Run - 0
	Current Score - 94/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : PP Chawla


	Ball - 6	Run - 0
	Current Score - 94/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : PP Chawla


Over: 13
	Ball - 1	Run - 1
	Current Score - 95/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 2	Run - 0
	Current Score - 95/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 3	Run - 4
	Current Score - 99/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 4	Run - 0
	Current Score - 99/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 5	Run - 4
	Current Score - 103/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 6	Run - 0
	Current Score - 103/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


Over: 14
	Ball - 1	Run - 2
	Current Score - 105/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : JO Holder


	Ball - 2	Run - 1
	Current Score - 106/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


	Ball - 3	Run - 1
	Current Score - 107/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : JO Holder


	Ball - 4	Run - 0
	Current Score - 107/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : JO Holder


	Ball - 5	Run - 1
	Current Score - 108/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


	Ball - 6	Run - 0
	Current Score - 108/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


Over: 15
	Ball - 1	Run - 0
	Current Score - 108/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 2	Run - 0
	Current Score - 108/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 3	Run - 6
	Current Score - 114/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : SP Narine


	Ball - 4	Run - 1
	Current Score - 115/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 5	Run - 0
	Current Score - 115/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


	Ball - 6	Run - 0
	Current Score - 115/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : SP Narine


Over: 16
	Ball - 1	Run - 0
	Current Score - 115/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : JO Holder


	Ball - 2	Run - 1
	Current Score - 116/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


	Ball - 3	Run - 6
	Current Score - 122/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


	Ball - 4	Run - 4
	Current Score - 126/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


	Ball - 5	Run - 4
	Current Score - 130/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


	Ball - 6	Run - 2
	Current Score - 132/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : JO Holder


Over: 17
	Ball - 1	Run - 0
	Current Score - 132/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 2	Run - 1
	Current Score - 133/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : UT Yadav


	Ball - 3	Run - 0
	Current Score - 133/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : UT Yadav


	Ball - 4	Run - 1
	Current Score - 134/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 5	Run - 0
	Current Score - 134/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 6	Run - 0
	Current Score - 134/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


Over: 18
	Ball - 1	Run - 1
	Current Score - 135/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 2	Run - 0
	Current Score - 135/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 3	Run - 1
	Current Score - 136/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 4	Run - 0
	Current Score - 136/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : GB Hogg


	Ball - 5	Run - 1
	Current Score - 137/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


	Ball - 6	Run - 0
	Current Score - 137/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : GB Hogg


Over: 19
	Ball - 1	Run - 0
	Current Score - 137/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : UT Yadav


	Ball - 2	Run - 1
	Current Score - 138/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 3	Run - 4
	Current Score - 142/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 4	Run - 0
	Current Score - 142/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 5	Run - 6
	Current Score - 148/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


	Ball - 6	Run - 0
	Current Score - 148/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : UT Yadav


Over: 20
	Ball - 1	Run - 1
	Current Score - 149/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 2	Run - 0
	Current Score - 149/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 3	Run - 0
	Current Score - 149/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 4	Run - 2
	Current Score - 151/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 5	Run - 4
	Current Score - 155/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : AD Russell


	Ball - 6	Run - 1
	Current Score - 156/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : AD Russell


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


	Ball - 2	Run - 1
	Current Score - 1/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 3	Run - 0
	Current Score - 1/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 4	Run - 0
	Current Score - 1/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 5	Run - 4
	Current Score - 5/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 6	Run - 1
	Current Score - 6/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


Over: 2
	Ball - 1	Run - 0
	Current Score - 6/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 6/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


	Ball - 3	Run - 1
	Current Score - 7/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 4	Run - 2
	Current Score - 9/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 5	Run - 4
	Current Score - 13/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 6	Run - 0
	Current Score - 13/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


Over: 3
	Ball - 1	Run - 1
	Current Score - 14/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


	Ball - 2	Run - 1
	Current Score - 15/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 3	Run - 4
	Current Score - 19/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 4	Run - 1
	Current Score - 20/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


	Ball - 5	Run - 0
	Current Score - 20/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


	Ball - 6	Run - 4
	Current Score - 24/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


Over: 4
	Ball - 1	Run - 4
	Current Score - 28/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 28/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


	Ball - 3	Run - 1
	Current Score - 29/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 4	Run - 0
	Current Score - 29/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 5	Run - 2
	Current Score - 31/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 6	Run - 1
	Current Score - 32/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


Over: 5
	Ball - 1	Run - 0
	Current Score - 32/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


	Ball - 2	Run - 1
	Current Score - 33/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 3	Run - 0
	Current Score - 33/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 4	Run - 1
	Current Score - 34/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Z Khan


	Ball - 5	Run - 1
	Current Score - 35/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


	Ball - 6	Run - 0
	Current Score - 35/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Z Khan


Over: 6
	Ball - 1	Run - 0
	Current Score - 35/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 35/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 3	Run - 1
	Current Score - 36/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


	Ball - 4	Run - 1
	Current Score - 37/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 5	Run - 0
	Current Score - 37/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : Mohammed Shami


	Ball - 6	Run - 1
	Current Score - 38/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : Mohammed Shami


Over: 7
	Ball - 1	Run - 0
	Current Score - 38/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CH Morris


	Ball - 2	Run - 0
	Current Score - 38/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CH Morris


	Ball - 3	Run - 0
	Current Score - 38/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CH Morris


	Ball - 4	Run - 1
	Current Score - 39/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : CH Morris


	Ball - 5	Run - 2
	Current Score - 41/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : CH Morris


	Ball - 6	Run - 1
	Current Score - 42/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CH Morris


Over: 8
	Ball - 1	Run - 1
	Current Score - 43/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CR Brathwaite


	Ball - 2	Run - 0
	Current Score - 43/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CR Brathwaite


	Ball - 3	Run - 1
	Current Score - 44/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : CR Brathwaite


	Ball - 4	Run - 0
	Current Score - 44/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : CR Brathwaite


	Ball - 5	Run - 1
	Current Score - 45/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : CR Brathwaite


	Ball - 6	Run - 0
	Current Score - 45/1
	Wicket!!!! 
	Striker : PP Chawla
	Non Striker : G Gambhir
	Bowler : CR Brathwaite


Over: 9
	Ball - 1	Run - 2
	Current Score - 47/1
	Striker : G Gambhir
	Non Striker : PP Chawla
	Bowler : CH Morris


	Ball - 2	Run - 0
	Current Score - 47/1
	Striker : G Gambhir
	Non Striker : PP Chawla
	Bowler : CH Morris


	Ball - 3	Run - 0
	Current Score - 47/1
	Striker : G Gambhir
	Non Striker : PP Chawla
	Bowler : CH Morris


	Ball - 4	Run - 1
	Current Score - 48/1
	Striker : PP Chawla
	Non Striker : G Gambhir
	Bowler : CH Morris


	Ball - 5	Run - 0
	Current Score - 48/1
	Striker : PP Chawla
	Non Striker : G Gambhir
	Bowler : CH Morris


	Ball - 6	Run - 0
	Current Score - 48/2
	Wicket!!!! 
	Striker : YK Pathan
	Non Striker : G Gambhir
	Bowler : CH Morris


Over: 10
	Ball - 1	Run - 0
	Current Score - 48/2
	Striker : G Gambhir
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 2	Run - 0
	Current Score - 48/2
	Striker : G Gambhir
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 3	Run - 0
	Current Score - 48/2
	Striker : G Gambhir
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 4	Run - 0
	Current Score - 48/2
	Striker : G Gambhir
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 5	Run - 0
	Current Score - 48/3
	Wicket!!!! 
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 6	Run - 0
	Current Score - 48/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


Over: 11
	Ball - 1	Run - 1
	Current Score - 49/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 2	Run - 2
	Current Score - 51/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 3	Run - 4
	Current Score - 55/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 4	Run - 1
	Current Score - 56/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 5	Run - 1
	Current Score - 57/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 6	Run - 4
	Current Score - 61/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


Over: 12
	Ball - 1	Run - 1
	Current Score - 62/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 2	Run - 1
	Current Score - 63/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : CR Brathwaite


	Ball - 3	Run - 1
	Current Score - 64/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 4	Run - 2
	Current Score - 66/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 5	Run - 0
	Current Score - 66/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 6	Run - 1
	Current Score - 67/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : CR Brathwaite


Over: 13
	Ball - 1	Run - 6
	Current Score - 73/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 2	Run - 0
	Current Score - 73/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 3	Run - 1
	Current Score - 74/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 4	Run - 1
	Current Score - 75/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 5	Run - 0
	Current Score - 75/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : A Mishra


	Ball - 6	Run - 1
	Current Score - 76/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : A Mishra


Over: 14
	Ball - 1	Run - 4
	Current Score - 80/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 2	Run - 1
	Current Score - 81/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : CR Brathwaite


	Ball - 3	Run - 1
	Current Score - 82/3
	Striker : SA Yadav
	Non Striker : YK Pathan
	Bowler : CR Brathwaite


	Ball - 4	Run - 1
	Current Score - 83/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : CR Brathwaite


	Ball - 5	Run - 0
	Current Score - 83/3
	Striker : YK Pathan
	Non Striker : SA Yadav
	Bowler : CR Brathwaite


	Ball - 6	Run - 0
	Current Score - 83/4
	Wicket!!!! 
	Striker : R Sathish
	Non Striker : SA Yadav
	Bowler : CR Brathwaite


Over: 15
	Ball - 1	Run - 1
	Current Score - 84/4
	Striker : R Sathish
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 2	Run - 0
	Current Score - 84/5
	Wicket!!!! 
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 3	Run - 1
	Current Score - 85/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : A Mishra


	Ball - 4	Run - 1
	Current Score - 86/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 5	Run - 0
	Current Score - 86/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 6	Run - 6
	Current Score - 92/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : A Mishra


Over: 16
	Ball - 1	Run - 4
	Current Score - 96/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 96/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : Mohammed Shami


	Ball - 3	Run - 0
	Current Score - 96/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : Mohammed Shami


	Ball - 4	Run - 1
	Current Score - 97/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Mohammed Shami


	Ball - 5	Run - 0
	Current Score - 97/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Mohammed Shami


	Ball - 6	Run - 2
	Current Score - 99/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Mohammed Shami


Over: 17
	Ball - 1	Run - 4
	Current Score - 103/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : A Mishra


	Ball - 2	Run - 2
	Current Score - 105/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : A Mishra


	Ball - 3	Run - 0
	Current Score - 105/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : A Mishra


	Ball - 4	Run - 1
	Current Score - 106/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 5	Run - 4
	Current Score - 110/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : A Mishra


	Ball - 6	Run - 1
	Current Score - 111/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : A Mishra


Over: 18
	Ball - 1	Run - 1
	Current Score - 112/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : CH Morris


	Ball - 2	Run - 1
	Current Score - 113/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 3	Run - 6
	Current Score - 119/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 4	Run - 0
	Current Score - 119/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 5	Run - 6
	Current Score - 125/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 6	Run - 0
	Current Score - 125/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


Over: 19
	Ball - 1	Run - 1
	Current Score - 126/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Z Khan


	Ball - 2	Run - 1
	Current Score - 127/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : Z Khan


	Ball - 3	Run - 1
	Current Score - 128/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Z Khan


	Ball - 4	Run - 4
	Current Score - 132/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Z Khan


	Ball - 5	Run - 0
	Current Score - 132/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : Z Khan


	Ball - 6	Run - 1
	Current Score - 133/5
	Striker : SA Yadav
	Non Striker : AD Russell
	Bowler : Z Khan


Over: 20
	Ball - 1	Run - 0
	Current Score - 133/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 2	Run - 0
	Current Score - 133/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 3	Run - 0
	Current Score - 133/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 4	Run - 0
	Current Score - 133/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 5	Run - 0
	Current Score - 133/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


	Ball - 6	Run - 2
	Current Score - 135/5
	Striker : AD Russell
	Non Striker : SA Yadav
	Bowler : CH Morris


PREDICTED : 

Team 1 Score  : 156
Team 2 Score  : 135

Winner :Team 1

Team 1
SV Samson-70
KK Nair-70
Q de Kock-12
SS Iyer-4


Team 2
SA Yadav-46
AD Russell-35
G Gambhir-27
RV Uthappa-21
YK Pathan-6
R Sathish-0
PP Chawla-0

ACTUAL : 
Team 1 186
Team 2 159

Innings 1 KK Nair-68
Innings 2 RV Uthappa-72
