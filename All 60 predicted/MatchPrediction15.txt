Match No. 15
Date: 2016-04-21
Gujarat Lions VS Sunrisers Hyderabad

Toss: 
Sunrisers Hyderabad won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : AJ Finch
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 2	Run - 0
	Current Score - 0/1
	Wicket!!!! 
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 0/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 0/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 0/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 6	Run - 1
	Current Score - 1/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : B Kumar


Over: 2
	Ball - 1	Run - 0
	Current Score - 1/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 2	Run - 2
	Current Score - 3/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 3	Run - 4
	Current Score - 7/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 4	Run - 4
	Current Score - 11/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 5	Run - 4
	Current Score - 15/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 6	Run - 6
	Current Score - 21/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


Over: 3
	Ball - 1	Run - 0
	Current Score - 21/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 2	Run - 4
	Current Score - 25/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 26/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 4	Run - 4
	Current Score - 30/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 5	Run - 2
	Current Score - 32/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


	Ball - 6	Run - 4
	Current Score - 36/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : B Kumar


Over: 4
	Ball - 1	Run - 6
	Current Score - 42/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 1
	Current Score - 43/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 1
	Current Score - 44/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 1
	Current Score - 45/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 1
	Current Score - 46/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 46/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : Mustafizur Rahman


Over: 5
	Ball - 1	Run - 1
	Current Score - 47/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 2	Run - 0
	Current Score - 47/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 3	Run - 4
	Current Score - 51/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 4	Run - 1
	Current Score - 52/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 5	Run - 0
	Current Score - 52/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : BB Sran


	Ball - 6	Run - 1
	Current Score - 53/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : BB Sran


Over: 6
	Ball - 1	Run - 1
	Current Score - 54/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 2	Run - 4
	Current Score - 58/1
	Striker : BB McCullum
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 3	Run - 1
	Current Score - 59/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : DJ Hooda


	Ball - 4	Run - 0
	Current Score - 59/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : DJ Hooda


	Ball - 5	Run - 0
	Current Score - 59/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : DJ Hooda


	Ball - 6	Run - 4
	Current Score - 63/1
	Striker : SK Raina
	Non Striker : BB McCullum
	Bowler : DJ Hooda


Over: 7
	Ball - 1	Run - 0
	Current Score - 63/2
	Wicket!!!! 
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 2	Run - 1
	Current Score - 64/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : MC Henriques


	Ball - 3	Run - 1
	Current Score - 65/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 4	Run - 4
	Current Score - 69/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 5	Run - 1
	Current Score - 70/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : MC Henriques


	Ball - 6	Run - 0
	Current Score - 70/2
	Striker : SK Raina
	Non Striker : KD Karthik
	Bowler : MC Henriques


Over: 8
	Ball - 1	Run - 4
	Current Score - 74/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 2	Run - 0
	Current Score - 74/2
	Striker : KD Karthik
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 3	Run - 0
	Current Score - 74/3
	Wicket!!!! 
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 4	Run - 0
	Current Score - 74/3
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 5	Run - 0
	Current Score - 74/3
	Striker : DJ Bravo
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 6	Run - 0
	Current Score - 74/4
	Wicket!!!! 
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : Bipul Sharma


Over: 9
	Ball - 1	Run - 0
	Current Score - 74/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 2	Run - 0
	Current Score - 74/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 3	Run - 1
	Current Score - 75/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 4	Run - 4
	Current Score - 79/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 5	Run - 0
	Current Score - 79/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : MC Henriques


	Ball - 6	Run - 1
	Current Score - 80/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


Over: 10
	Ball - 1	Run - 1
	Current Score - 81/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : DJ Hooda


	Ball - 2	Run - 0
	Current Score - 81/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : DJ Hooda


	Ball - 3	Run - 1
	Current Score - 82/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 4	Run - 2
	Current Score - 84/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 5	Run - 0
	Current Score - 84/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 6	Run - 0
	Current Score - 84/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


Over: 11
	Ball - 1	Run - 0
	Current Score - 84/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Bipul Sharma


	Ball - 2	Run - 1
	Current Score - 85/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 3	Run - 0
	Current Score - 85/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 4	Run - 1
	Current Score - 86/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Bipul Sharma


	Ball - 5	Run - 1
	Current Score - 87/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : Bipul Sharma


	Ball - 6	Run - 4
	Current Score - 91/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : Bipul Sharma


Over: 12
	Ball - 1	Run - 0
	Current Score - 91/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : DJ Hooda


	Ball - 2	Run - 2
	Current Score - 93/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : DJ Hooda


	Ball - 3	Run - 4
	Current Score - 97/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : DJ Hooda


	Ball - 4	Run - 1
	Current Score - 98/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 5	Run - 0
	Current Score - 98/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


	Ball - 6	Run - 4
	Current Score - 102/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : DJ Hooda


Over: 13
	Ball - 1	Run - 0
	Current Score - 102/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 2	Run - 0
	Current Score - 102/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 3	Run - 2
	Current Score - 104/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 4	Run - 6
	Current Score - 110/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 5	Run - 0
	Current Score - 110/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : MC Henriques


	Ball - 6	Run - 1
	Current Score - 111/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : MC Henriques


Over: 14
	Ball - 1	Run - 1
	Current Score - 112/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 2	Run - 2
	Current Score - 114/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 3	Run - 3
	Current Score - 117/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : BB Sran


	Ball - 4	Run - 1
	Current Score - 118/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : BB Sran


	Ball - 5	Run - 1
	Current Score - 119/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : BB Sran


	Ball - 6	Run - 0
	Current Score - 119/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : BB Sran


Over: 15
	Ball - 1	Run - 1
	Current Score - 120/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 120/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 4
	Current Score - 124/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 1
	Current Score - 125/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 1
	Current Score - 126/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 126/4
	Striker : SK Raina
	Non Striker : RA Jadeja
	Bowler : Mustafizur Rahman


Over: 16
	Ball - 1	Run - 4
	Current Score - 130/4
	Striker : RA Jadeja
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 2	Run - 0
	Current Score - 130/5
	Wicket!!!! 
	Striker : AD Nath
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 3	Run - 4
	Current Score - 134/5
	Striker : AD Nath
	Non Striker : SK Raina
	Bowler : B Kumar


	Ball - 4	Run - 1
	Current Score - 135/5
	Striker : SK Raina
	Non Striker : AD Nath
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 135/5
	Striker : SK Raina
	Non Striker : AD Nath
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 135/6
	Wicket!!!! 
	Striker : DW Steyn
	Non Striker : AD Nath
	Bowler : B Kumar


Over: 17
	Ball - 1	Run - 0
	Current Score - 135/6
	Striker : AD Nath
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 135/7
	Wicket!!!! 
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 135/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 0
	Current Score - 135/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 1
	Current Score - 136/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 136/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : Mustafizur Rahman


Over: 18
	Ball - 1	Run - 1
	Current Score - 137/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : BB Sran


	Ball - 2	Run - 0
	Current Score - 137/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : BB Sran


	Ball - 3	Run - 0
	Current Score - 137/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : BB Sran


	Ball - 4	Run - 0
	Current Score - 137/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : BB Sran


	Ball - 5	Run - 1
	Current Score - 138/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : BB Sran


	Ball - 6	Run - 1
	Current Score - 139/7
	Striker : DW Steyn
	Non Striker : P Kumar
	Bowler : BB Sran


Over: 19
	Ball - 1	Run - 6
	Current Score - 145/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 145/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 145/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 4
	Current Score - 149/7
	Striker : P Kumar
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 0
	Current Score - 149/8
	Wicket!!!! 
	Striker : DS Kulkarni
	Non Striker : DW Steyn
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 1
	Current Score - 150/8
	Striker : DW Steyn
	Non Striker : DS Kulkarni
	Bowler : Mustafizur Rahman


Over: 20
	Ball - 1	Run - 4
	Current Score - 154/8
	Striker : DS Kulkarni
	Non Striker : DW Steyn
	Bowler : B Kumar


	Ball - 2	Run - 2
	Current Score - 156/8
	Striker : DS Kulkarni
	Non Striker : DW Steyn
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 156/8
	Striker : DS Kulkarni
	Non Striker : DW Steyn
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 156/9
	Wicket!!!! 
	Striker : PV Tambe
	Non Striker : DW Steyn
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 156/9
	Striker : PV Tambe
	Non Striker : DW Steyn
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 156/9
	Striker : PV Tambe
	Non Striker : DW Steyn
	Bowler : B Kumar


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DW Steyn


	Ball - 2	Run - 1
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DW Steyn


	Ball - 3	Run - 0
	Current Score - 1/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DW Steyn


	Ball - 4	Run - 1
	Current Score - 2/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DW Steyn


	Ball - 5	Run - 4
	Current Score - 6/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DW Steyn


	Ball - 6	Run - 0
	Current Score - 6/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DW Steyn


Over: 2
	Ball - 1	Run - 0
	Current Score - 6/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 2	Run - 1
	Current Score - 7/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 3	Run - 6
	Current Score - 13/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 4	Run - 1
	Current Score - 14/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 5	Run - 0
	Current Score - 14/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 6	Run - 1
	Current Score - 15/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


Over: 3
	Ball - 1	Run - 6
	Current Score - 21/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DW Steyn


	Ball - 2	Run - 6
	Current Score - 27/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DW Steyn


	Ball - 3	Run - 0
	Current Score - 27/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DW Steyn


	Ball - 4	Run - 4
	Current Score - 31/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DW Steyn


	Ball - 5	Run - 1
	Current Score - 32/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DW Steyn


	Ball - 6	Run - 0
	Current Score - 32/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : DW Steyn


Over: 4
	Ball - 1	Run - 1
	Current Score - 33/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 2	Run - 4
	Current Score - 37/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 3	Run - 0
	Current Score - 37/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 4	Run - 4
	Current Score - 41/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


	Ball - 5	Run - 1
	Current Score - 42/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : P Kumar


	Ball - 6	Run - 1
	Current Score - 43/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : P Kumar


Over: 5
	Ball - 1	Run - 0
	Current Score - 43/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 2	Run - 4
	Current Score - 47/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 3	Run - 0
	Current Score - 47/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 4	Run - 0
	Current Score - 47/1
	Wicket!!!! 
	Striker : MC Henriques
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 5	Run - 0
	Current Score - 47/1
	Striker : MC Henriques
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 6	Run - 1
	Current Score - 48/1
	Striker : DA Warner
	Non Striker : MC Henriques
	Bowler : DJ Bravo


Over: 6
	Ball - 1	Run - 0
	Current Score - 48/2
	Wicket!!!! 
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : SK Raina


	Ball - 2	Run - 6
	Current Score - 54/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : SK Raina


	Ball - 3	Run - 1
	Current Score - 55/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : SK Raina


	Ball - 4	Run - 0
	Current Score - 55/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : SK Raina


	Ball - 5	Run - 6
	Current Score - 61/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : SK Raina


	Ball - 6	Run - 4
	Current Score - 65/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : SK Raina


Over: 7
	Ball - 1	Run - 2
	Current Score - 67/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


	Ball - 2	Run - 2
	Current Score - 69/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


	Ball - 3	Run - 1
	Current Score - 70/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : PV Tambe


	Ball - 4	Run - 1
	Current Score - 71/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


	Ball - 5	Run - 1
	Current Score - 72/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : PV Tambe


	Ball - 6	Run - 1
	Current Score - 73/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


Over: 8
	Ball - 1	Run - 4
	Current Score - 77/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : SK Raina


	Ball - 2	Run - 1
	Current Score - 78/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : SK Raina


	Ball - 3	Run - 4
	Current Score - 82/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : SK Raina


	Ball - 4	Run - 4
	Current Score - 86/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : SK Raina


	Ball - 5	Run - 1
	Current Score - 87/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : SK Raina


	Ball - 6	Run - 1
	Current Score - 88/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : SK Raina


Over: 9
	Ball - 1	Run - 1
	Current Score - 89/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 2	Run - 1
	Current Score - 90/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : DJ Bravo


	Ball - 3	Run - 0
	Current Score - 90/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : DJ Bravo


	Ball - 4	Run - 0
	Current Score - 90/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : DJ Bravo


	Ball - 5	Run - 0
	Current Score - 90/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 90/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : DJ Bravo


Over: 10
	Ball - 1	Run - 1
	Current Score - 91/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : PV Tambe


	Ball - 2	Run - 0
	Current Score - 91/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : PV Tambe


	Ball - 3	Run - 0
	Current Score - 91/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : PV Tambe


	Ball - 4	Run - 1
	Current Score - 92/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


	Ball - 5	Run - 0
	Current Score - 92/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


	Ball - 6	Run - 0
	Current Score - 92/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : PV Tambe


Over: 11
	Ball - 1	Run - 0
	Current Score - 92/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : RA Jadeja


	Ball - 2	Run - 0
	Current Score - 92/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 93/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : RA Jadeja


	Ball - 4	Run - 0
	Current Score - 93/2
	Striker : EJG Morgan
	Non Striker : DA Warner
	Bowler : RA Jadeja


	Ball - 5	Run - 1
	Current Score - 94/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : RA Jadeja


	Ball - 6	Run - 0
	Current Score - 94/2
	Striker : DA Warner
	Non Striker : EJG Morgan
	Bowler : RA Jadeja


Over: 12
	Ball - 1	Run - 0
	Current Score - 94/3
	Wicket!!!! 
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 2	Run - 0
	Current Score - 94/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 95/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DJ Bravo


	Ball - 4	Run - 1
	Current Score - 96/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : DJ Bravo


	Ball - 5	Run - 1
	Current Score - 97/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DJ Bravo


	Ball - 6	Run - 0
	Current Score - 97/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DJ Bravo


Over: 13
	Ball - 1	Run - 0
	Current Score - 97/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : RA Jadeja


	Ball - 2	Run - 1
	Current Score - 98/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : RA Jadeja


	Ball - 3	Run - 0
	Current Score - 98/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : RA Jadeja


	Ball - 4	Run - 1
	Current Score - 99/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : RA Jadeja


	Ball - 5	Run - 1
	Current Score - 100/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : RA Jadeja


	Ball - 6	Run - 1
	Current Score - 101/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : RA Jadeja


Over: 14
	Ball - 1	Run - 2
	Current Score - 103/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 2	Run - 0
	Current Score - 103/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 3	Run - 0
	Current Score - 103/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 4	Run - 0
	Current Score - 103/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 5	Run - 0
	Current Score - 103/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 6	Run - 0
	Current Score - 103/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


Over: 15
	Ball - 1	Run - 0
	Current Score - 103/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : RA Jadeja


	Ball - 2	Run - 0
	Current Score - 103/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : RA Jadeja


	Ball - 3	Run - 1
	Current Score - 104/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : RA Jadeja


	Ball - 4	Run - 0
	Current Score - 104/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : RA Jadeja


	Ball - 5	Run - 0
	Current Score - 104/3
	Striker : DA Warner
	Non Striker : DJ Hooda
	Bowler : RA Jadeja


	Ball - 6	Run - 1
	Current Score - 105/3
	Striker : DJ Hooda
	Non Striker : DA Warner
	Bowler : RA Jadeja


Over: 16
	Ball - 1	Run - 0
	Current Score - 105/4
	Wicket!!!! 
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DJ Bravo


	Ball - 2	Run - 4
	Current Score - 109/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DJ Bravo


	Ball - 3	Run - 1
	Current Score - 110/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DJ Bravo


	Ball - 4	Run - 4
	Current Score - 114/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DJ Bravo


	Ball - 5	Run - 0
	Current Score - 114/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DJ Bravo


	Ball - 6	Run - 2
	Current Score - 116/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DJ Bravo


Over: 17
	Ball - 1	Run - 6
	Current Score - 122/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : SK Raina


	Ball - 2	Run - 1
	Current Score - 123/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : SK Raina


	Ball - 3	Run - 0
	Current Score - 123/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : SK Raina


	Ball - 4	Run - 1
	Current Score - 124/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : SK Raina


	Ball - 5	Run - 0
	Current Score - 124/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : SK Raina


	Ball - 6	Run - 6
	Current Score - 130/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : SK Raina


Over: 18
	Ball - 1	Run - 4
	Current Score - 134/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : PV Tambe


	Ball - 2	Run - 0
	Current Score - 134/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : PV Tambe


	Ball - 3	Run - 1
	Current Score - 135/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : PV Tambe


	Ball - 4	Run - 1
	Current Score - 136/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : PV Tambe


	Ball - 5	Run - 4
	Current Score - 140/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : PV Tambe


	Ball - 6	Run - 2
	Current Score - 142/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : PV Tambe


Over: 19
	Ball - 1	Run - 0
	Current Score - 142/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 2	Run - 1
	Current Score - 143/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DS Kulkarni


	Ball - 3	Run - 0
	Current Score - 143/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DS Kulkarni


	Ball - 4	Run - 2
	Current Score - 145/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DS Kulkarni


	Ball - 5	Run - 1
	Current Score - 146/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


	Ball - 6	Run - 0
	Current Score - 146/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DS Kulkarni


Over: 20
	Ball - 1	Run - 1
	Current Score - 147/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DW Steyn


	Ball - 2	Run - 0
	Current Score - 147/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DW Steyn


	Ball - 3	Run - 0
	Current Score - 147/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DW Steyn


	Ball - 4	Run - 1
	Current Score - 148/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DW Steyn


	Ball - 5	Run - 1
	Current Score - 149/4
	Striker : AP Tare
	Non Striker : DJ Hooda
	Bowler : DW Steyn


	Ball - 6	Run - 1
	Current Score - 150/4
	Striker : DJ Hooda
	Non Striker : AP Tare
	Bowler : DW Steyn


PREDICTED : 

Team 1 Score  : 156
Team 2 Score  : 150

Winner :Team 1

Team 1
SK Raina-68
RA Jadeja-29
BB McCullum-23
P Kumar-13
KD Karthik-10
DS Kulkarni-7
AD Nath-5
DW Steyn-1
DJ Bravo-0
PV Tambe-0
AJ Finch-0


Team 2
DA Warner-48
DJ Hooda-28
S Dhawan-26
EJG Morgan-25
AP Tare-22
MC Henriques-1

ACTUAL : 
Team 1 135
Team 2 137

Innings 1 SK Raina-71
Innings 2 DA Warner-74
