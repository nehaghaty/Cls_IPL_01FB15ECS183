Match No. 11
Date: 2016-04-17
Royal Challengers Bangalore VS Delhi Daredevils

Toss: 
Delhi Daredevils won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : Z Khan


	Ball - 2	Run - 1
	Current Score - 1/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : Z Khan


	Ball - 3	Run - 0
	Current Score - 1/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : Z Khan


	Ball - 4	Run - 4
	Current Score - 5/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : Z Khan


	Ball - 5	Run - 1
	Current Score - 6/0
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : Z Khan


	Ball - 6	Run - 4
	Current Score - 10/0
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : Z Khan


Over: 2
	Ball - 1	Run - 2
	Current Score - 12/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : CH Morris


	Ball - 2	Run - 0
	Current Score - 12/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : CH Morris


	Ball - 3	Run - 0
	Current Score - 12/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : CH Morris


	Ball - 4	Run - 0
	Current Score - 12/0
	Striker : V Kohli
	Non Striker : CH Gayle
	Bowler : CH Morris


	Ball - 5	Run - 1
	Current Score - 13/0
	Striker : CH Gayle
	Non Striker : V Kohli
	Bowler : CH Morris


	Ball - 6	Run - 0
	Current Score - 13/1
	Wicket!!!! 
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : CH Morris


Over: 3
	Ball - 1	Run - 0
	Current Score - 13/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 2	Run - 4
	Current Score - 17/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 3	Run - 0
	Current Score - 17/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 4	Run - 0
	Current Score - 17/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 5	Run - 1
	Current Score - 18/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : Z Khan


	Ball - 6	Run - 1
	Current Score - 19/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


Over: 4
	Ball - 1	Run - 0
	Current Score - 19/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 19/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : Mohammed Shami


	Ball - 3	Run - 0
	Current Score - 19/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : Mohammed Shami


	Ball - 4	Run - 1
	Current Score - 20/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Mohammed Shami


	Ball - 5	Run - 6
	Current Score - 26/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Mohammed Shami


	Ball - 6	Run - 2
	Current Score - 28/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Mohammed Shami


Over: 5
	Ball - 1	Run - 1
	Current Score - 29/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 2	Run - 0
	Current Score - 29/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 3	Run - 1
	Current Score - 30/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : Z Khan


	Ball - 4	Run - 1
	Current Score - 31/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 5	Run - 0
	Current Score - 31/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : Z Khan


	Ball - 6	Run - 1
	Current Score - 32/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : Z Khan


Over: 6
	Ball - 1	Run - 0
	Current Score - 32/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : CH Morris


	Ball - 2	Run - 1
	Current Score - 33/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : CH Morris


	Ball - 3	Run - 1
	Current Score - 34/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : CH Morris


	Ball - 4	Run - 2
	Current Score - 36/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : CH Morris


	Ball - 5	Run - 4
	Current Score - 40/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : CH Morris


	Ball - 6	Run - 2
	Current Score - 42/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : CH Morris


Over: 7
	Ball - 1	Run - 0
	Current Score - 42/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 2	Run - 4
	Current Score - 46/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 3	Run - 0
	Current Score - 46/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 4	Run - 0
	Current Score - 46/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 5	Run - 1
	Current Score - 47/1
	Striker : V Kohli
	Non Striker : AB de Villiers
	Bowler : P Negi


	Ball - 6	Run - 1
	Current Score - 48/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : P Negi


Over: 8
	Ball - 1	Run - 1
	Current Score - 49/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 2	Run - 0
	Current Score - 49/1
	Striker : AB de Villiers
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 3	Run - 0
	Current Score - 49/2
	Wicket!!!! 
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 4	Run - 1
	Current Score - 50/2
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : A Mishra


	Ball - 5	Run - 4
	Current Score - 54/2
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : A Mishra


	Ball - 6	Run - 1
	Current Score - 55/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : A Mishra


Over: 9
	Ball - 1	Run - 4
	Current Score - 59/2
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Negi


	Ball - 2	Run - 0
	Current Score - 59/2
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Negi


	Ball - 3	Run - 1
	Current Score - 60/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 4	Run - 1
	Current Score - 61/2
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : P Negi


	Ball - 5	Run - 1
	Current Score - 62/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 6	Run - 2
	Current Score - 64/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : P Negi


Over: 10
	Ball - 1	Run - 1
	Current Score - 65/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 2	Run - 0
	Current Score - 65/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 3	Run - 1
	Current Score - 66/2
	Striker : V Kohli
	Non Striker : SR Watson
	Bowler : A Mishra


	Ball - 4	Run - 1
	Current Score - 67/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 5	Run - 0
	Current Score - 67/2
	Striker : SR Watson
	Non Striker : V Kohli
	Bowler : A Mishra


	Ball - 6	Run - 0
	Current Score - 67/3
	Wicket!!!! 
	Striker : SN Khan
	Non Striker : V Kohli
	Bowler : A Mishra


Over: 11
	Ball - 1	Run - 0
	Current Score - 67/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : P Negi


	Ball - 2	Run - 2
	Current Score - 69/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : P Negi


	Ball - 3	Run - 1
	Current Score - 70/3
	Striker : SN Khan
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 4	Run - 3
	Current Score - 73/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : P Negi


	Ball - 5	Run - 1
	Current Score - 74/3
	Striker : SN Khan
	Non Striker : V Kohli
	Bowler : P Negi


	Ball - 6	Run - 3
	Current Score - 77/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : P Negi


Over: 12
	Ball - 1	Run - 1
	Current Score - 78/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : CR Brathwaite


	Ball - 2	Run - 0
	Current Score - 78/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : CR Brathwaite


	Ball - 3	Run - 1
	Current Score - 79/3
	Striker : SN Khan
	Non Striker : V Kohli
	Bowler : CR Brathwaite


	Ball - 4	Run - 1
	Current Score - 80/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : CR Brathwaite


	Ball - 5	Run - 4
	Current Score - 84/3
	Striker : V Kohli
	Non Striker : SN Khan
	Bowler : CR Brathwaite


	Ball - 6	Run - 1
	Current Score - 85/3
	Striker : SN Khan
	Non Striker : V Kohli
	Bowler : CR Brathwaite


Over: 13
	Ball - 1	Run - 0
	Current Score - 85/4
	Wicket!!!! 
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 85/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Mohammed Shami


	Ball - 3	Run - 1
	Current Score - 86/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : Mohammed Shami


	Ball - 4	Run - 6
	Current Score - 92/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : Mohammed Shami


	Ball - 5	Run - 0
	Current Score - 92/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : Mohammed Shami


	Ball - 6	Run - 2
	Current Score - 94/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : Mohammed Shami


Over: 14
	Ball - 1	Run - 1
	Current Score - 95/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : CR Brathwaite


	Ball - 2	Run - 6
	Current Score - 101/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : CR Brathwaite


	Ball - 3	Run - 6
	Current Score - 107/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : CR Brathwaite


	Ball - 4	Run - 4
	Current Score - 111/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : CR Brathwaite


	Ball - 5	Run - 6
	Current Score - 117/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : CR Brathwaite


	Ball - 6	Run - 4
	Current Score - 121/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : CR Brathwaite


Over: 15
	Ball - 1	Run - 4
	Current Score - 125/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : A Mishra


	Ball - 2	Run - 1
	Current Score - 126/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : A Mishra


	Ball - 3	Run - 4
	Current Score - 130/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : A Mishra


	Ball - 4	Run - 4
	Current Score - 134/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : A Mishra


	Ball - 5	Run - 4
	Current Score - 138/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : A Mishra


	Ball - 6	Run - 6
	Current Score - 144/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : A Mishra


Over: 16
	Ball - 1	Run - 0
	Current Score - 144/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Z Khan


	Ball - 2	Run - 0
	Current Score - 144/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Z Khan


	Ball - 3	Run - 4
	Current Score - 148/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Z Khan


	Ball - 4	Run - 0
	Current Score - 148/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Z Khan


	Ball - 5	Run - 0
	Current Score - 148/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Z Khan


	Ball - 6	Run - 0
	Current Score - 148/4
	Striker : KM Jadhav
	Non Striker : SN Khan
	Bowler : Z Khan


Over: 17
	Ball - 1	Run - 6
	Current Score - 154/4
	Striker : SN Khan
	Non Striker : KM Jadhav
	Bowler : Mohammed Shami


	Ball - 2	Run - 0
	Current Score - 154/5
	Wicket!!!! 
	Striker : D Wiese
	Non Striker : KM Jadhav
	Bowler : Mohammed Shami


	Ball - 3	Run - 1
	Current Score - 155/5
	Striker : KM Jadhav
	Non Striker : D Wiese
	Bowler : Mohammed Shami


	Ball - 4	Run - 0
	Current Score - 155/5
	Striker : KM Jadhav
	Non Striker : D Wiese
	Bowler : Mohammed Shami


	Ball - 5	Run - 0
	Current Score - 155/5
	Striker : KM Jadhav
	Non Striker : D Wiese
	Bowler : Mohammed Shami


	Ball - 6	Run - 0
	Current Score - 155/6
	Wicket!!!! 
	Striker : Parvez Rasool
	Non Striker : D Wiese
	Bowler : Mohammed Shami


Over: 18
	Ball - 1	Run - 0
	Current Score - 155/6
	Striker : D Wiese
	Non Striker : Parvez Rasool
	Bowler : CH Morris


	Ball - 2	Run - 0
	Current Score - 155/6
	Striker : D Wiese
	Non Striker : Parvez Rasool
	Bowler : CH Morris


	Ball - 3	Run - 1
	Current Score - 156/6
	Striker : Parvez Rasool
	Non Striker : D Wiese
	Bowler : CH Morris


	Ball - 4	Run - 0
	Current Score - 156/6
	Striker : Parvez Rasool
	Non Striker : D Wiese
	Bowler : CH Morris


	Ball - 5	Run - 2
	Current Score - 158/6
	Striker : Parvez Rasool
	Non Striker : D Wiese
	Bowler : CH Morris


	Ball - 6	Run - 4
	Current Score - 162/6
	Striker : Parvez Rasool
	Non Striker : D Wiese
	Bowler : CH Morris


Over: 19
	Ball - 1	Run - 2
	Current Score - 164/6
	Striker : D Wiese
	Non Striker : Parvez Rasool
	Bowler : Mohammed Shami


	Ball - 2	Run - 2
	Current Score - 166/6
	Striker : D Wiese
	Non Striker : Parvez Rasool
	Bowler : Mohammed Shami


	Ball - 3	Run - 1
	Current Score - 167/6
	Striker : Parvez Rasool
	Non Striker : D Wiese
	Bowler : Mohammed Shami


	Ball - 4	Run - 0
	Current Score - 167/7
	Wicket!!!! 
	Striker : HV Patel
	Non Striker : D Wiese
	Bowler : Mohammed Shami


	Ball - 5	Run - 1
	Current Score - 168/7
	Striker : D Wiese
	Non Striker : HV Patel
	Bowler : Mohammed Shami


	Ball - 6	Run - 0
	Current Score - 168/7
	Striker : D Wiese
	Non Striker : HV Patel
	Bowler : Mohammed Shami


Over: 20
	Ball - 1	Run - 0
	Current Score - 168/7
	Striker : HV Patel
	Non Striker : D Wiese
	Bowler : CH Morris


	Ball - 2	Run - 0
	Current Score - 168/8
	Wicket!!!! 
	Striker : YS Chahal
	Non Striker : D Wiese
	Bowler : CH Morris


	Ball - 3	Run - 0
	Current Score - 168/9
	Wicket!!!! 
	Striker : S Aravind
	Non Striker : D Wiese
	Bowler : CH Morris


	Ball - 4	Run - 0
	Current Score - 168/10

	All Out!################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 1
	Current Score - 1/0
	Striker : SS Iyer
	Non Striker : Q de Kock
	Bowler : S Aravind


	Ball - 2	Run - 1
	Current Score - 2/0
	Striker : Q de Kock
	Non Striker : SS Iyer
	Bowler : S Aravind


	Ball - 3	Run - 4
	Current Score - 6/0
	Striker : Q de Kock
	Non Striker : SS Iyer
	Bowler : S Aravind


	Ball - 4	Run - 0
	Current Score - 6/1
	Wicket!!!! 
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : S Aravind


	Ball - 5	Run - 1
	Current Score - 7/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : S Aravind


	Ball - 6	Run - 0
	Current Score - 7/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : S Aravind


Over: 2
	Ball - 1	Run - 1
	Current Score - 8/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 2	Run - 0
	Current Score - 8/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 3	Run - 0
	Current Score - 8/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 4	Run - 0
	Current Score - 8/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 5	Run - 0
	Current Score - 8/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 6	Run - 0
	Current Score - 8/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


Over: 3
	Ball - 1	Run - 2
	Current Score - 10/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : S Aravind


	Ball - 2	Run - 0
	Current Score - 10/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : S Aravind


	Ball - 3	Run - 2
	Current Score - 12/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : S Aravind


	Ball - 4	Run - 1
	Current Score - 13/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : S Aravind


	Ball - 5	Run - 0
	Current Score - 13/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : S Aravind


	Ball - 6	Run - 1
	Current Score - 14/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : S Aravind


Over: 4
	Ball - 1	Run - 0
	Current Score - 14/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 15/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


	Ball - 3	Run - 4
	Current Score - 19/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


	Ball - 4	Run - 0
	Current Score - 19/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 19/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


	Ball - 6	Run - 4
	Current Score - 23/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


Over: 5
	Ball - 1	Run - 0
	Current Score - 23/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 2	Run - 4
	Current Score - 27/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 3	Run - 4
	Current Score - 31/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 4	Run - 0
	Current Score - 31/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 5	Run - 0
	Current Score - 31/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 6	Run - 0
	Current Score - 31/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : Parvez Rasool


Over: 6
	Ball - 1	Run - 1
	Current Score - 32/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 33/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 34/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : SR Watson


	Ball - 4	Run - 0
	Current Score - 34/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 34/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : SR Watson


	Ball - 6	Run - 1
	Current Score - 35/1
	Striker : SV Samson
	Non Striker : SS Iyer
	Bowler : SR Watson


Over: 7
	Ball - 1	Run - 0
	Current Score - 35/1
	Striker : SS Iyer
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 2	Run - 0
	Current Score - 35/2
	Wicket!!!! 
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 3	Run - 4
	Current Score - 39/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 39/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 5	Run - 0
	Current Score - 39/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 40/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : YS Chahal


Over: 8
	Ball - 1	Run - 0
	Current Score - 40/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 2	Run - 0
	Current Score - 40/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 3	Run - 0
	Current Score - 40/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 4	Run - 4
	Current Score - 44/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 5	Run - 0
	Current Score - 44/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 6	Run - 0
	Current Score - 44/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


Over: 9
	Ball - 1	Run - 0
	Current Score - 44/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 2	Run - 2
	Current Score - 46/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 3	Run - 2
	Current Score - 48/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 4	Run - 1
	Current Score - 49/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : HV Patel


	Ball - 5	Run - 2
	Current Score - 51/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : HV Patel


	Ball - 6	Run - 4
	Current Score - 55/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : HV Patel


Over: 10
	Ball - 1	Run - 4
	Current Score - 59/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 2	Run - 0
	Current Score - 59/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 3	Run - 0
	Current Score - 59/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 4	Run - 1
	Current Score - 60/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 5	Run - 2
	Current Score - 62/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : D Wiese


	Ball - 6	Run - 1
	Current Score - 63/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


Over: 11
	Ball - 1	Run - 1
	Current Score - 64/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : Parvez Rasool


	Ball - 2	Run - 2
	Current Score - 66/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : Parvez Rasool


	Ball - 3	Run - 6
	Current Score - 72/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : Parvez Rasool


	Ball - 4	Run - 1
	Current Score - 73/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 5	Run - 0
	Current Score - 73/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : Parvez Rasool


	Ball - 6	Run - 0
	Current Score - 73/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : Parvez Rasool


Over: 12
	Ball - 1	Run - 1
	Current Score - 74/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : HV Patel


	Ball - 2	Run - 1
	Current Score - 75/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 3	Run - 0
	Current Score - 75/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 4	Run - 0
	Current Score - 75/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 5	Run - 0
	Current Score - 75/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 6	Run - 1
	Current Score - 76/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : HV Patel


Over: 13
	Ball - 1	Run - 0
	Current Score - 76/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : YS Chahal


	Ball - 2	Run - 1
	Current Score - 77/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 3	Run - 0
	Current Score - 77/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 4	Run - 0
	Current Score - 77/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


	Ball - 5	Run - 1
	Current Score - 78/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 79/2
	Striker : KK Nair
	Non Striker : SV Samson
	Bowler : YS Chahal


Over: 14
	Ball - 1	Run - 2
	Current Score - 81/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 2	Run - 0
	Current Score - 81/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 3	Run - 0
	Current Score - 81/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 4	Run - 0
	Current Score - 81/2
	Striker : SV Samson
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 5	Run - 0
	Current Score - 81/3
	Wicket!!!! 
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 6	Run - 1
	Current Score - 82/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : D Wiese


Over: 15
	Ball - 1	Run - 0
	Current Score - 82/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 83/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : SR Watson


	Ball - 3	Run - 1
	Current Score - 84/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : SR Watson


	Ball - 4	Run - 1
	Current Score - 85/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : SR Watson


	Ball - 5	Run - 1
	Current Score - 86/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 86/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : SR Watson


Over: 16
	Ball - 1	Run - 0
	Current Score - 86/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : S Aravind


	Ball - 2	Run - 1
	Current Score - 87/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : S Aravind


	Ball - 3	Run - 0
	Current Score - 87/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : S Aravind


	Ball - 4	Run - 0
	Current Score - 87/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : S Aravind


	Ball - 5	Run - 1
	Current Score - 88/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : S Aravind


	Ball - 6	Run - 0
	Current Score - 88/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : S Aravind


Over: 17
	Ball - 1	Run - 2
	Current Score - 90/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 2	Run - 0
	Current Score - 90/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : HV Patel


	Ball - 3	Run - 1
	Current Score - 91/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : HV Patel


	Ball - 4	Run - 0
	Current Score - 91/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : HV Patel


	Ball - 5	Run - 4
	Current Score - 95/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : HV Patel


	Ball - 6	Run - 2
	Current Score - 97/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : HV Patel


Over: 18
	Ball - 1	Run - 0
	Current Score - 97/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 2	Run - 0
	Current Score - 97/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 3	Run - 0
	Current Score - 97/3
	Striker : JP Duminy
	Non Striker : KK Nair
	Bowler : D Wiese


	Ball - 4	Run - 1
	Current Score - 98/3
	Striker : KK Nair
	Non Striker : JP Duminy
	Bowler : D Wiese


	Ball - 5	Run - 0
	Current Score - 98/4
	Wicket!!!! 
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : D Wiese


	Ball - 6	Run - 0
	Current Score - 98/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : D Wiese


Over: 19
	Ball - 1	Run - 0
	Current Score - 98/4
	Striker : JP Duminy
	Non Striker : CR Brathwaite
	Bowler : SR Watson


	Ball - 2	Run - 1
	Current Score - 99/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : SR Watson


	Ball - 3	Run - 0
	Current Score - 99/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : SR Watson


	Ball - 4	Run - 2
	Current Score - 101/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : SR Watson


	Ball - 5	Run - 0
	Current Score - 101/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : SR Watson


	Ball - 6	Run - 0
	Current Score - 101/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : SR Watson


Over: 20
	Ball - 1	Run - 4
	Current Score - 105/4
	Striker : JP Duminy
	Non Striker : CR Brathwaite
	Bowler : YS Chahal


	Ball - 2	Run - 2
	Current Score - 107/4
	Striker : JP Duminy
	Non Striker : CR Brathwaite
	Bowler : YS Chahal


	Ball - 3	Run - 6
	Current Score - 113/4
	Striker : JP Duminy
	Non Striker : CR Brathwaite
	Bowler : YS Chahal


	Ball - 4	Run - 1
	Current Score - 114/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : YS Chahal


	Ball - 5	Run - 2
	Current Score - 116/4
	Striker : CR Brathwaite
	Non Striker : JP Duminy
	Bowler : YS Chahal


	Ball - 6	Run - 1
	Current Score - 117/4
	Striker : JP Duminy
	Non Striker : CR Brathwaite
	Bowler : YS Chahal


PREDICTED : 

Team 1 Score  : 168
Team 2 Score  : 117

Winner :Team 1

Team 1
SN Khan-66
V Kohli-57
KM Jadhav-11
AB de Villiers-10
D Wiese-7
Parvez Rasool-6
CH Gayle-5
SR Watson-5
HV Patel-1
YS Chahal-0
S Aravind-0


Team 2
SV Samson-42
KK Nair-30
JP Duminy-22
SS Iyer-13
CR Brathwaite-5
Q de Kock-5

ACTUAL : 
Team 1 191
Team 2 192

Innings 1 V Kohli-75
Innings 2 Q de Kock-104
