Match No. 55
Date: 2016-05-22
Kolkata Knight Riders VS Sunrisers Hyderabad

Toss: 
Sunrisers Hyderabad won the toss and chose to field

################################## Innings 1 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : B Kumar


	Ball - 2	Run - 0
	Current Score - 0/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 1/0
	Striker : G Gambhir
	Non Striker : RV Uthappa
	Bowler : B Kumar


	Ball - 4	Run - 1
	Current Score - 2/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 2/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 2/0
	Striker : RV Uthappa
	Non Striker : G Gambhir
	Bowler : B Kumar


Over: 2
	Ball - 1	Run - 0
	Current Score - 2/1
	Wicket!!!! 
	Striker : C Munro
	Non Striker : RV Uthappa
	Bowler : BB Sran


	Ball - 2	Run - 2
	Current Score - 4/1
	Striker : C Munro
	Non Striker : RV Uthappa
	Bowler : BB Sran


	Ball - 3	Run - 0
	Current Score - 4/1
	Striker : C Munro
	Non Striker : RV Uthappa
	Bowler : BB Sran


	Ball - 4	Run - 1
	Current Score - 5/1
	Striker : RV Uthappa
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 5	Run - 0
	Current Score - 5/2
	Wicket!!!! 
	Striker : MK Pandey
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 6	Run - 0
	Current Score - 5/3
	Wicket!!!! 
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


Over: 3
	Ball - 1	Run - 1
	Current Score - 6/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : B Kumar


	Ball - 2	Run - 4
	Current Score - 10/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : B Kumar


	Ball - 3	Run - 4
	Current Score - 14/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 14/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : B Kumar


	Ball - 5	Run - 1
	Current Score - 15/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : B Kumar


	Ball - 6	Run - 0
	Current Score - 15/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : B Kumar


Over: 4
	Ball - 1	Run - 4
	Current Score - 19/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 2	Run - 2
	Current Score - 21/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 3	Run - 0
	Current Score - 21/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 4	Run - 0
	Current Score - 21/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 5	Run - 2
	Current Score - 23/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


	Ball - 6	Run - 0
	Current Score - 23/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : BB Sran


Over: 5
	Ball - 1	Run - 0
	Current Score - 23/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : KS Williamson


	Ball - 2	Run - 0
	Current Score - 23/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : KS Williamson


	Ball - 3	Run - 2
	Current Score - 25/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : KS Williamson


	Ball - 4	Run - 1
	Current Score - 26/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KS Williamson


	Ball - 5	Run - 1
	Current Score - 27/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : KS Williamson


	Ball - 6	Run - 1
	Current Score - 28/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KS Williamson


Over: 6
	Ball - 1	Run - 1
	Current Score - 29/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : DJ Hooda


	Ball - 2	Run - 0
	Current Score - 29/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : DJ Hooda


	Ball - 3	Run - 1
	Current Score - 30/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : DJ Hooda


	Ball - 4	Run - 4
	Current Score - 34/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : DJ Hooda


	Ball - 5	Run - 2
	Current Score - 36/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : DJ Hooda


	Ball - 6	Run - 0
	Current Score - 36/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : DJ Hooda


Over: 7
	Ball - 1	Run - 0
	Current Score - 36/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KV Sharma


	Ball - 2	Run - 0
	Current Score - 36/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KV Sharma


	Ball - 3	Run - 0
	Current Score - 36/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KV Sharma


	Ball - 4	Run - 0
	Current Score - 36/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KV Sharma


	Ball - 5	Run - 0
	Current Score - 36/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KV Sharma


	Ball - 6	Run - 4
	Current Score - 40/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : KV Sharma


Over: 8
	Ball - 1	Run - 0
	Current Score - 40/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : DJ Hooda


	Ball - 2	Run - 1
	Current Score - 41/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : DJ Hooda


	Ball - 3	Run - 0
	Current Score - 41/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : DJ Hooda


	Ball - 4	Run - 2
	Current Score - 43/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : DJ Hooda


	Ball - 5	Run - 1
	Current Score - 44/3
	Striker : C Munro
	Non Striker : YK Pathan
	Bowler : DJ Hooda


	Ball - 6	Run - 1
	Current Score - 45/3
	Striker : YK Pathan
	Non Striker : C Munro
	Bowler : DJ Hooda


Over: 9
	Ball - 1	Run - 0
	Current Score - 45/4
	Wicket!!!! 
	Striker : JO Holder
	Non Striker : YK Pathan
	Bowler : KV Sharma


	Ball - 2	Run - 1
	Current Score - 46/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : KV Sharma


	Ball - 3	Run - 2
	Current Score - 48/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : KV Sharma


	Ball - 4	Run - 4
	Current Score - 52/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : KV Sharma


	Ball - 5	Run - 0
	Current Score - 52/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : KV Sharma


	Ball - 6	Run - 4
	Current Score - 56/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : KV Sharma


Over: 10
	Ball - 1	Run - 1
	Current Score - 57/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : MC Henriques


	Ball - 2	Run - 4
	Current Score - 61/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : MC Henriques


	Ball - 3	Run - 0
	Current Score - 61/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : MC Henriques


	Ball - 4	Run - 0
	Current Score - 61/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : MC Henriques


	Ball - 5	Run - 1
	Current Score - 62/4
	Striker : JO Holder
	Non Striker : YK Pathan
	Bowler : MC Henriques


	Ball - 6	Run - 3
	Current Score - 65/4
	Striker : YK Pathan
	Non Striker : JO Holder
	Bowler : MC Henriques


Over: 11
	Ball - 1	Run - 4
	Current Score - 69/4
	Striker : JO Holder
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 69/4
	Striker : JO Holder
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 69/4
	Striker : JO Holder
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 4
	Current Score - 73/4
	Striker : JO Holder
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 0
	Current Score - 73/5
	Wicket!!!! 
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 73/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


Over: 12
	Ball - 1	Run - 1
	Current Score - 74/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : MC Henriques


	Ball - 2	Run - 0
	Current Score - 74/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : MC Henriques


	Ball - 3	Run - 1
	Current Score - 75/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : MC Henriques


	Ball - 4	Run - 0
	Current Score - 75/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : MC Henriques


	Ball - 5	Run - 2
	Current Score - 77/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : MC Henriques


	Ball - 6	Run - 1
	Current Score - 78/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : MC Henriques


Over: 13
	Ball - 1	Run - 1
	Current Score - 79/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 0
	Current Score - 79/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 1
	Current Score - 80/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 1
	Current Score - 81/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 6
	Current Score - 87/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 1
	Current Score - 88/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : Mustafizur Rahman


Over: 14
	Ball - 1	Run - 0
	Current Score - 88/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : KV Sharma


	Ball - 2	Run - 0
	Current Score - 88/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : KV Sharma


	Ball - 3	Run - 0
	Current Score - 88/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : KV Sharma


	Ball - 4	Run - 6
	Current Score - 94/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : KV Sharma


	Ball - 5	Run - 0
	Current Score - 94/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : KV Sharma


	Ball - 6	Run - 0
	Current Score - 94/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : KV Sharma


Over: 15
	Ball - 1	Run - 1
	Current Score - 95/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : BB Sran


	Ball - 2	Run - 0
	Current Score - 95/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : BB Sran


	Ball - 3	Run - 1
	Current Score - 96/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 4	Run - 0
	Current Score - 96/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 5	Run - 0
	Current Score - 96/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 6	Run - 0
	Current Score - 96/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


Over: 16
	Ball - 1	Run - 1
	Current Score - 97/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : B Kumar


	Ball - 2	Run - 1
	Current Score - 98/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : B Kumar


	Ball - 3	Run - 0
	Current Score - 98/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : B Kumar


	Ball - 4	Run - 0
	Current Score - 98/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : B Kumar


	Ball - 5	Run - 1
	Current Score - 99/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : B Kumar


	Ball - 6	Run - 2
	Current Score - 101/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : B Kumar


Over: 17
	Ball - 1	Run - 1
	Current Score - 102/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 1
	Current Score - 103/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 103/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 2
	Current Score - 105/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 0
	Current Score - 105/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 0
	Current Score - 105/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : Mustafizur Rahman


Over: 18
	Ball - 1	Run - 0
	Current Score - 105/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 2	Run - 0
	Current Score - 105/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 3	Run - 0
	Current Score - 105/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 4	Run - 2
	Current Score - 107/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 5	Run - 0
	Current Score - 107/5
	Striker : YK Pathan
	Non Striker : Shakib Al Hasan
	Bowler : BB Sran


	Ball - 6	Run - 1
	Current Score - 108/5
	Striker : Shakib Al Hasan
	Non Striker : YK Pathan
	Bowler : BB Sran


Over: 19
	Ball - 1	Run - 0
	Current Score - 108/6
	Wicket!!!! 
	Striker : SA Yadav
	Non Striker : Shakib Al Hasan
	Bowler : Mustafizur Rahman


	Ball - 2	Run - 1
	Current Score - 109/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : Mustafizur Rahman


	Ball - 3	Run - 0
	Current Score - 109/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : Mustafizur Rahman


	Ball - 4	Run - 2
	Current Score - 111/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : Mustafizur Rahman


	Ball - 5	Run - 0
	Current Score - 111/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : Mustafizur Rahman


	Ball - 6	Run - 2
	Current Score - 113/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : Mustafizur Rahman


Over: 20
	Ball - 1	Run - 0
	Current Score - 113/6
	Striker : SA Yadav
	Non Striker : Shakib Al Hasan
	Bowler : B Kumar


	Ball - 2	Run - 0
	Current Score - 113/6
	Striker : SA Yadav
	Non Striker : Shakib Al Hasan
	Bowler : B Kumar


	Ball - 3	Run - 1
	Current Score - 114/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : B Kumar


	Ball - 4	Run - 2
	Current Score - 116/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : B Kumar


	Ball - 5	Run - 0
	Current Score - 116/6
	Striker : Shakib Al Hasan
	Non Striker : SA Yadav
	Bowler : B Kumar


	Ball - 6	Run - 1
	Current Score - 117/6
	Striker : SA Yadav
	Non Striker : Shakib Al Hasan
	Bowler : B Kumar


################################## Innings 2 ######################################

Over: 1
	Ball - 1	Run - 0
	Current Score - 0/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : YK Pathan


	Ball - 2	Run - 1
	Current Score - 1/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : YK Pathan


	Ball - 3	Run - 4
	Current Score - 5/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : YK Pathan


	Ball - 4	Run - 0
	Current Score - 5/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : YK Pathan


	Ball - 5	Run - 1
	Current Score - 6/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : YK Pathan


	Ball - 6	Run - 0
	Current Score - 6/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : YK Pathan


Over: 2
	Ball - 1	Run - 2
	Current Score - 8/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 2	Run - 0
	Current Score - 8/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 3	Run - 2
	Current Score - 10/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 4	Run - 1
	Current Score - 11/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : AS Rajpoot


	Ball - 5	Run - 1
	Current Score - 12/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 6	Run - 4
	Current Score - 16/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


Over: 3
	Ball - 1	Run - 4
	Current Score - 20/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : Shakib Al Hasan


	Ball - 2	Run - 4
	Current Score - 24/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : Shakib Al Hasan


	Ball - 3	Run - 0
	Current Score - 24/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : Shakib Al Hasan


	Ball - 4	Run - 0
	Current Score - 24/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : Shakib Al Hasan


	Ball - 5	Run - 1
	Current Score - 25/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : Shakib Al Hasan


	Ball - 6	Run - 0
	Current Score - 25/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : Shakib Al Hasan


Over: 4
	Ball - 1	Run - 0
	Current Score - 25/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 2	Run - 2
	Current Score - 27/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 3	Run - 0
	Current Score - 27/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 4	Run - 0
	Current Score - 27/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 5	Run - 2
	Current Score - 29/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 6	Run - 4
	Current Score - 33/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


Over: 5
	Ball - 1	Run - 0
	Current Score - 33/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 2	Run - 0
	Current Score - 33/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 3	Run - 4
	Current Score - 37/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 4	Run - 0
	Current Score - 37/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


	Ball - 5	Run - 1
	Current Score - 38/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : AS Rajpoot


	Ball - 6	Run - 1
	Current Score - 39/0
	Striker : DA Warner
	Non Striker : S Dhawan
	Bowler : AS Rajpoot


Over: 6
	Ball - 1	Run - 0
	Current Score - 39/0
	Striker : S Dhawan
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 2	Run - 0
	Current Score - 39/1
	Wicket!!!! 
	Striker : NV Ojha
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 3	Run - 0
	Current Score - 39/1
	Striker : NV Ojha
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 4	Run - 2
	Current Score - 41/1
	Striker : NV Ojha
	Non Striker : DA Warner
	Bowler : SP Narine


	Ball - 5	Run - 1
	Current Score - 42/1
	Striker : DA Warner
	Non Striker : NV Ojha
	Bowler : SP Narine


	Ball - 6	Run - 0
	Current Score - 42/2
	Wicket!!!! 
	Striker : Yuvraj Singh
	Non Striker : NV Ojha
	Bowler : SP Narine


Over: 7
	Ball - 1	Run - 0
	Current Score - 42/2
	Striker : NV Ojha
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


	Ball - 2	Run - 1
	Current Score - 43/2
	Striker : Yuvraj Singh
	Non Striker : NV Ojha
	Bowler : JO Holder


	Ball - 3	Run - 2
	Current Score - 45/2
	Striker : Yuvraj Singh
	Non Striker : NV Ojha
	Bowler : JO Holder


	Ball - 4	Run - 1
	Current Score - 46/2
	Striker : NV Ojha
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


	Ball - 5	Run - 1
	Current Score - 47/2
	Striker : Yuvraj Singh
	Non Striker : NV Ojha
	Bowler : JO Holder


	Ball - 6	Run - 2
	Current Score - 49/2
	Striker : Yuvraj Singh
	Non Striker : NV Ojha
	Bowler : JO Holder


Over: 8
	Ball - 1	Run - 0
	Current Score - 49/3
	Wicket!!!! 
	Striker : KS Williamson
	Non Striker : Yuvraj Singh
	Bowler : C Munro


	Ball - 2	Run - 0
	Current Score - 49/4
	Wicket!!!! 
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : C Munro


	Ball - 3	Run - 1
	Current Score - 50/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : C Munro


	Ball - 4	Run - 1
	Current Score - 51/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : C Munro


	Ball - 5	Run - 1
	Current Score - 52/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : C Munro


	Ball - 6	Run - 0
	Current Score - 52/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : C Munro


Over: 9
	Ball - 1	Run - 0
	Current Score - 52/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


	Ball - 2	Run - 1
	Current Score - 53/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : JO Holder


	Ball - 3	Run - 1
	Current Score - 54/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


	Ball - 4	Run - 0
	Current Score - 54/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


	Ball - 5	Run - 0
	Current Score - 54/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


	Ball - 6	Run - 2
	Current Score - 56/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : JO Holder


Over: 10
	Ball - 1	Run - 1
	Current Score - 57/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 2	Run - 0
	Current Score - 57/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 3	Run - 1
	Current Score - 58/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 4	Run - 6
	Current Score - 64/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 5	Run - 1
	Current Score - 65/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 6	Run - 0
	Current Score - 65/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


Over: 11
	Ball - 1	Run - 1
	Current Score - 66/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 2	Run - 4
	Current Score - 70/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 3	Run - 4
	Current Score - 74/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 4	Run - 0
	Current Score - 74/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 5	Run - 1
	Current Score - 75/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Shakib Al Hasan


	Ball - 6	Run - 1
	Current Score - 76/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


Over: 12
	Ball - 1	Run - 1
	Current Score - 77/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 2	Run - 0
	Current Score - 77/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 3	Run - 1
	Current Score - 78/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 4	Run - 0
	Current Score - 78/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 5	Run - 1
	Current Score - 79/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 6	Run - 1
	Current Score - 80/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


Over: 13
	Ball - 1	Run - 4
	Current Score - 84/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : SP Narine


	Ball - 2	Run - 4
	Current Score - 88/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : SP Narine


	Ball - 3	Run - 6
	Current Score - 94/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : SP Narine


	Ball - 4	Run - 1
	Current Score - 95/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : SP Narine


	Ball - 5	Run - 1
	Current Score - 96/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : SP Narine


	Ball - 6	Run - 2
	Current Score - 98/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : SP Narine


Over: 14
	Ball - 1	Run - 0
	Current Score - 98/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 2	Run - 1
	Current Score - 99/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 3	Run - 0
	Current Score - 99/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 4	Run - 0
	Current Score - 99/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Kuldeep Yadav


	Ball - 5	Run - 1
	Current Score - 100/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 6	Run - 0
	Current Score - 100/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


Over: 15
	Ball - 1	Run - 1
	Current Score - 101/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Shakib Al Hasan


	Ball - 2	Run - 1
	Current Score - 102/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 3	Run - 0
	Current Score - 102/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 4	Run - 1
	Current Score - 103/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Shakib Al Hasan


	Ball - 5	Run - 1
	Current Score - 104/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


	Ball - 6	Run - 0
	Current Score - 104/4
	Striker : DJ Hooda
	Non Striker : Yuvraj Singh
	Bowler : Shakib Al Hasan


Over: 16
	Ball - 1	Run - 0
	Current Score - 104/4
	Striker : Yuvraj Singh
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 2	Run - 0
	Current Score - 104/5
	Wicket!!!! 
	Striker : MC Henriques
	Non Striker : DJ Hooda
	Bowler : Kuldeep Yadav


	Ball - 3	Run - 1
	Current Score - 105/5
	Striker : DJ Hooda
	Non Striker : MC Henriques
	Bowler : Kuldeep Yadav


	Ball - 4	Run - 0
	Current Score - 105/5
	Striker : DJ Hooda
	Non Striker : MC Henriques
	Bowler : Kuldeep Yadav


	Ball - 5	Run - 0
	Current Score - 105/6
	Wicket!!!! 
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : Kuldeep Yadav


	Ball - 6	Run - 0
	Current Score - 105/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : Kuldeep Yadav


Over: 17
	Ball - 1	Run - 1
	Current Score - 106/6
	Striker : KV Sharma
	Non Striker : MC Henriques
	Bowler : Shakib Al Hasan


	Ball - 2	Run - 0
	Current Score - 106/7
	Wicket!!!! 
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : Shakib Al Hasan


	Ball - 3	Run - 6
	Current Score - 112/7
	Striker : B Kumar
	Non Striker : MC Henriques
	Bowler : Shakib Al Hasan


	Ball - 4	Run - 1
	Current Score - 113/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : Shakib Al Hasan


	Ball - 5	Run - 6
	Current Score - 119/7
	Striker : MC Henriques
	Non Striker : B Kumar
	Bowler : Shakib Al Hasan


PREDICTED : 

Team 1 Score  : 117
Team 2 Score  : 119

Winner :Team 2

Team 1
YK Pathan-55
Shakib Al Hasan-28
C Munro-17
JO Holder-13
SA Yadav-2
G Gambhir-1
RV Uthappa-1
MK Pandey-0


Team 2
DJ Hooda-37
Yuvraj Singh-23
S Dhawan-20
DA Warner-19
MC Henriques-8
B Kumar-7
NV Ojha-5
KS Williamson-0
KV Sharma-0

ACTUAL : 
Team 1 171
Team 2 149

Innings 1 YK Pathan-51
Innings 2 S Dhawan-50
